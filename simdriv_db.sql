-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 24, 2021 at 03:42 PM
-- Server version: 5.7.24
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simdriv_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

DROP TABLE IF EXISTS `driver`;
CREATE TABLE IF NOT EXISTS `driver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` varchar(50) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `foto` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `no_sim` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`id`, `nama`, `tgl_lahir`, `jk`, `tempat_lahir`, `alamat`, `foto`, `status`, `no_sim`, `email`, `password`) VALUES
(1, 'Ujang', '1999-08-11', 'Laki-laki', 'Bekasi', 'Cianjur, Jawa Barat', 'f9d41a4d781e8b315830fefba6b28f88.jpg', 1, '1782685817245', 'ujang@driver.com', '$2y$10$jBXpx6LcpkElfKxr1XJfZO9DJZuB/.fcs.uC6Hie8Gw7.rAIpwFla');

-- --------------------------------------------------------

--
-- Table structure for table `hak_akses`
--

DROP TABLE IF EXISTS `hak_akses`;
CREATE TABLE IF NOT EXISTS `hak_akses` (
  `id_akses` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_akses`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hak_akses`
--

INSERT INTO `hak_akses` (`id_akses`, `level`) VALUES
(1, 'Super Admin'),
(2, 'Sub Admin');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE IF NOT EXISTS `jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `nama_jabatan`) VALUES
(1, 'Manager'),
(2, 'Marketing');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

DROP TABLE IF EXISTS `karyawan`;
CREATE TABLE IF NOT EXISTS `karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `alamat` text,
  `foto` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `no_sim` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `jabatan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `nip`, `nama`, `tempat_lahir`, `tgl_lahir`, `jk`, `alamat`, `foto`, `status`, `no_sim`, `email`, `password`, `jabatan`) VALUES
(1, 'NIP001', 'Desta', 'Bekasi', '1999-06-15', 'Laki-laki', 'Kp. Tanah Baru', 'a9fd7f9072eae89a090b7ad8a17264ed.jpg', 1, '3469182301', 'desta@markom.com', '$2y$10$Omq7nbNFfYY7u2atCH4rju3Srbfmd3B6XRsNFqXaE7cQbQ3i1FtjC', 2),
(2, 'NIP002', 'Desti', 'Bekasi', '2000-07-28', 'Perempuan', 'Kp. Walahir Cikarang Utara', '057c7a0bcf8fdf5f12c62a68e5b54aa2.jpg', 1, '2349935793', 'desti@markom.com', '$2y$10$XASl.puv2bElbDngVJoim.IZmhK3ZQ/QNtNwP6BbHkVJPdt9hm8ky', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan`
--

DROP TABLE IF EXISTS `pengajuan`;
CREATE TABLE IF NOT EXISTS `pengajuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_pengajuan` varchar(50) NOT NULL,
  `tgl_pengajuan` date NOT NULL,
  `karyawan` int(11) NOT NULL,
  `tujuan` text NOT NULL,
  `tgl_berangkat` date NOT NULL,
  `keperluan` text NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajuan`
--

INSERT INTO `pengajuan` (`id`, `no_pengajuan`, `tgl_pengajuan`, `karyawan`, `tujuan`, `tgl_berangkat`, `keperluan`, `status`) VALUES
(1, 'APP00001', '2021-07-01', 2, 'Ruko CBD BLOK A', '2021-07-24', 'Meeting dengan Customer', 1),
(2, 'APP00002', '2021-07-24', 2, 'Living Plaza', '2021-07-24', 'Jalan-jalan', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran`
--

DROP TABLE IF EXISTS `pengeluaran`;
CREATE TABLE IF NOT EXISTS `pengeluaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perjalanan` int(11) DEFAULT NULL,
  `nama_pengeluaran` varchar(255) DEFAULT NULL,
  `jml_pengeluaran` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran`
--

INSERT INTO `pengeluaran` (`id`, `perjalanan`, `nama_pengeluaran`, `jml_pengeluaran`) VALUES
(1, 1, 'Bensin', 200000),
(2, 1, 'Tol', 30000),
(5, 2, 'Bensin', 150000),
(6, 2, 'Tol', 50000);

-- --------------------------------------------------------

--
-- Table structure for table `perjalanan`
--

DROP TABLE IF EXISTS `perjalanan`;
CREATE TABLE IF NOT EXISTS `perjalanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pengajuan` int(11) NOT NULL DEFAULT '0',
  `driver` int(11) NOT NULL DEFAULT '0',
  `vehicle` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perjalanan`
--

INSERT INTO `perjalanan` (`id`, `pengajuan`, `driver`, `vehicle`, `status`) VALUES
(1, 1, 1, 2, 1),
(2, 2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '0',
  `nama` varchar(100) NOT NULL DEFAULT '0',
  `password` varchar(255) DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `nama`, `password`, `status`, `level`) VALUES
(1, 'admin', 'admin', '$2y$10$z9ixY2W/RdecI4NvFrCDJuPtOMgb0utrW.qDq1E.T7NQ9VZXLQGSi', 1, 1),
(2, 'subadmin', 'subadmin', '$2y$10$HcibP5H2HHtHlT2/0/Gbo.V3uSa4es7Tt14VeR26CoKQdFJKICnn6', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE IF NOT EXISTS `vehicle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_plat` varchar(10) NOT NULL DEFAULT '0',
  `merk` varchar(50) NOT NULL DEFAULT '0',
  `tahun_perakitan` year(4) NOT NULL DEFAULT '2000',
  `isi_silinder` varchar(5) NOT NULL DEFAULT '0',
  `no_rangka` varchar(50) NOT NULL DEFAULT '0',
  `no_mesin` varchar(50) NOT NULL DEFAULT '0',
  `no_bpkb` varchar(50) NOT NULL DEFAULT '0',
  `warna` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`id`, `no_plat`, `merk`, `tahun_perakitan`, `isi_silinder`, `no_rangka`, `no_mesin`, `no_bpkb`, `warna`) VALUES
(1, 'B9072OPZ', 'AVANZA', 2016, '1000', 'B38798AD', 'UAHSIDU2893A', 'ASDH27687', 'HITAM'),
(2, 'B4455OP', 'AVANZA', 2016, '28397', 'ASDA123', '1231323', 'ASDASD', 'HITAM');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
