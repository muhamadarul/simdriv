<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Beranda extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('Beranda_model');
        $this->load->model('Auth_model');
        $this->load->model('Perjalanan_model');
    		if (!$this->session->userdata('username')) {
    						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
    						redirect('auth');
    		}
    }
    public function index()
    {
      $data['title'] = "Beranda";
      $data['vehicle'] = $this->Beranda_model->countVehicle();
      $data['karyawan'] = $this->Beranda_model->countKaryawan();
      $data['driver'] = $this->Beranda_model->countDriver();
      $data['user'] = $this->Auth_model->success_login();
      $today = date('y-m-d');
      $data['perjalanan'] = $this->Perjalanan_model->getToday($today);
      $this->load->view('templates/header',$data);
      $this->load->view('beranda/index',$data);
      $this->load->view('templates/footer');
    }
}
