<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('Karyawan_model');
        $this->load->model('Jabatan_model');
        $this->load->model('Auth_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
        }
    }
    public function index()
    {
      $data['title'] = "Karyawan";
      $data['user'] = $this->Auth_model->success_login();
      $this->load->view('templates/header',$data);
      $this->load->view('karyawan/index');
      $this->load->view('templates/footer');
    }
    public function ajax_list()
    {
        $list = $this->Karyawan_model->get_datatables();
        $data = array();
        if(isset($_POST['start']) && isset($_POST['draw']))
        {
          $no = $_POST['start'];
        }else{
             // need start and draw
             die(); // some error
        }
        foreach ($list as $karyawan) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $karyawan->nip;
            $row[] = $karyawan->nama;
            $row[] = $karyawan->nama_jabatan;
            $row[] = $karyawan->email;
            if ($karyawan->status == 1) {
              $status = '<span class="label label-success">Active</span>';
            }else {
              $status = '<span class="label label-danger">Deactive</span>';
            }
            $row[] = $status;
            //add html for action
            $row[] = '<a class="btn btn-default btn-outline-success btn-sm" href="javascript:void(0)" title="Edit" onclick="edit('."'".$karyawan->id_karyawan."'".')"><i class="fa fa-edit"></i></a>
                      <a class="btn btn-default btn-outline-warning btn-sm" href="javascript:void(0)" title="Detail" onclick="detail('."'".$karyawan->id_karyawan."'".')"><i class="fa fa-eye"></i></a>
                      <a class="btn btn-default btn-outline-danger btn-sm" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$karyawan->id_karyawan."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Karyawan_model->count_all(),
                        "recordsFiltered" => $this->Karyawan_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    private function _uploadImage()
    {
      //upload foto
      $config['upload_path']          = './assets/img/karyawan';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['encrypt_name'] = TRUE; //enkripsi file name upload
      $config['overwrite']            = true;
      $config['max_size']             = 520; // 500kb
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;
      $this->load->library('upload', $config);
      if ($this->upload->do_upload('foto')) {
          $a = array('upload_data' => $this->upload->data());
          $image= $a['upload_data']['file_name'];
          // $gambar = $_FILES['userfile']['name'];
      }else {
          $image= 'default.jpg';
          $foto = $this->upload->display_errors();
      }
    }
    public function tambah()
    {
      $nip = $this->input->post('nip');
      $nama = $this->input->post('nama');
      $tempat_lahir = $this->input->post('tempat_lahir');
      $tgl_lahir = $this->input->post('tgl_lahir');
      $jk  = $this->input->post('jk');
      $alamat = $this->input->post('alamat');
      $email = $this->input->post('email');
      $status = $this->input->post('status');
      $password  = $this->input->post('password');
      $no_sim  = $this->input->post('no_sim');
      $jabatan  = $this->input->post('jabatan');
      //upload foto
      $config['upload_path']          = './assets/img/karyawan';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['encrypt_name']         = TRUE; //enkripsi file name upload
      $config['overwrite']            = true;
      $config['max_size']             = 520; // 500kb
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;
      $this->load->library('upload', $config);
      if ($this->upload->do_upload('foto')) {
          $a = array('upload_data' => $this->upload->data());
          $image= $a['upload_data']['file_name'];
          // $gambar = $_FILES['userfile']['name'];
      }else {
          $image= 'default.jpg';
      }
      $error_upload = $this->upload->display_errors();
      if ($nip=='') {
        $result['pesan'] ="NIP Harus Diisi";
      }elseif ($nama=='') {
        $result['pesan'] ="Nama Harus Diisi";
      }elseif ($tempat_lahir=='') {
        $result['pesan'] ="Tempat Lahir Harus Diisi";
      }elseif ($tgl_lahir=='') {
        $result['pesan'] ="Tanggal Lahir Harus Diisi";
      }elseif ($jk=='') {
        $result['pesan'] ="JK Harus Diisi";
      }elseif ($alamat=='') {
        $result['pesan'] ="Alamat Harus Diisi";
      }elseif ($email=='') {
        $result['pesan'] ="Email Harus Diisi";
      }elseif ($status=='') {
        $result['pesan'] ="Status Harus Diisi";
      }elseif ($no_sim=='') {
        $result['pesan'] ="No sim Harus Diisi";
      }elseif ($jabatan=='') {
        $result['pesan'] ="Jabatan Harus Diisi";
      }elseif($error_upload !='') {
        $result['pesan'] = $error_upload;
      }else {
        $result['pesan']="";
        $data = array(
                'nip' => $nip,
                'nama' => $nama,
                'tempat_lahir' => $tempat_lahir,
                'tgl_lahir' => $tgl_lahir,
                'jk' => $jk,
                'alamat' => $alamat,
                'foto' => $image,
                'status' => $status,
                'no_sim' => $no_sim,
                'email' => $email,
                'password' => password_hash($password, PASSWORD_DEFAULT),
                'jabatan' => $jabatan,
              );
          $this->Karyawan_model->tambah($data);
      }
      echo json_encode($result);
   }
   public function hapus()
   {
     $id = $this->input->post('id');
     $this->Karyawan_model->hapus($id);
   }
   public function ajax_edit($id)
   {
     $data = $this->Karyawan_model->getById($id);
     echo json_encode($data);
   }
   public function ubah()
   {
     $id = $this->input->post('id');
     $nip = $this->input->post('nip2');
     $nama = $this->input->post('nama2');
     $tempat_lahir = $this->input->post('tempat_lahir2');
     $tgl_lahir = $this->input->post('tgl_lahir2');
     $jk  = $this->input->post('jk2');
     $alamat = $this->input->post('alamat2');
     $email = $this->input->post('email2');
     $status = $this->input->post('status2');
     $password  = $this->input->post('password2');
     $no_sim  = $this->input->post('no_sim2');
     $jabatan  = $this->input->post('jabatan2');
     $image2  = $this->input->post('foto_lama');
     if ($nip=='') {
       $result['pesan'] ="NIP Harus Diisi";
     }elseif ($nama=='') {
       $result['pesan'] ="Nama Harus Diisi";
     }elseif ($tempat_lahir=='') {
       $result['pesan'] ="Tempat Lahir Harus Diisi";
     }elseif ($tgl_lahir=='') {
       $result['pesan'] ="Tanggal Lahir Harus Diisi";
     }elseif ($jk=='') {
       $result['pesan'] ="JK Harus Diisi";
     }elseif ($alamat=='') {
       $result['pesan'] ="Alamat Harus Diisi";
     }elseif ($email=='') {
       $result['pesan'] ="Email Harus Diisi";
     }elseif ($status=='') {
       $result['pesan'] ="Status Harus Diisi";
     }elseif ($no_sim=='') {
       $result['pesan'] ="No sim Harus Diisi";
     }elseif ($jabatan=='') {
       $result['pesan'] ="Jabatan Harus Diisi";
     }else {
       if ($password=='' && (empty($_FILES['foto2']['name']))) {
         $data = array(
                 'nip' => $nip,
                 'nama' => $nama,
                 'tempat_lahir' => $tempat_lahir,
                 'tgl_lahir' => $tgl_lahir,
                 'jk' => $jk,
                 'alamat' => $alamat,
                 'email' => $email,
                 'status' => $status,
                 'no_sim' => $no_sim,
                 'jabatan' => $jabatan,
                 'foto' => $image2,
               );
           $result['pesan']="";
           $this->Karyawan_model->ubah($data,$id);
       }elseif ($password !='' && (!empty($_FILES['foto2']['name']))) {
         //upload foto
         $config['upload_path']          = './assets/img/karyawan';
         $config['allowed_types']        = 'gif|jpg|png|jpeg';
         $config['encrypt_name']         = TRUE; //enkripsi file name upload
         $config['overwrite']            = true;
         $config['max_size']             = 520; // 500kb
         // $config['max_width']            = 1024;
         // $config['max_height']           = 768;
         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto2')) {
             $a = array('upload_data' => $this->upload->data());
             $image= $a['upload_data']['file_name'];
             // $gambar = $_FILES['userfile']['name'];
         }else {
             $image= 'default.jpg';
         }
         $error_upload = $this->upload->display_errors();
         //jika upload gagal
           if($error_upload !='') {
           $result['pesan'] = $error_upload;
           //upload berhasil
           }else {
             $data = array(
                 'nip' => $nip,
                 'nama' => $nama,
                 'tempat_lahir' => $tempat_lahir,
                 'tgl_lahir' => $tgl_lahir,
                 'jk' => $jk,
                 'alamat' => $alamat,
                 'email' => $email,
                 'status' => $status,
                 'no_sim' => $no_sim,
                 'jabatan' => $jabatan,
                 'password' => password_hash($password, PASSWORD_DEFAULT),
                 'foto' => $image,
               );
             $result['pesan']="";
             $this->Karyawan_model->ubah($data,$id);
           }
       }elseif ($password =='' && (!empty($_FILES['foto2']['name']))) {
         //upload foto
         $config['upload_path']          = './assets/img/karyawan';
         $config['allowed_types']        = 'gif|jpg|png|jpeg';
         $config['encrypt_name']         = TRUE; //enkripsi file name upload
         $config['overwrite']            = true;
         $config['max_size']             = 520; // 500kb
         // $config['max_width']            = 1024;
         // $config['max_height']           = 768;
         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto2')) {
             $a = array('upload_data' => $this->upload->data());
             $image= $a['upload_data']['file_name'];
             // $gambar = $_FILES['userfile']['name'];
         }else {
             $image= 'default.jpg';
         }
         $error_upload = $this->upload->display_errors();
         //jika upload gagal
           if($error_upload !='') {
           $result['pesan'] = $error_upload;
           //upload berhasil
           }else {
             $data = array(
               'nip' => $nip,
               'nama' => $nama,
               'tempat_lahir' => $tempat_lahir,
               'tgl_lahir' => $tgl_lahir,
               'jk' => $jk,
               'alamat' => $alamat,
               'email' => $email,
               'status' => $status,
               'no_sim' => $no_sim,
               'jabatan' => $jabatan,
               'foto' => $image,
               );
             $result['pesan']="";
             $this->Karyawan_model->ubah($data,$id);
           }
       }else {
         $data = array(
           'nip' => $nip,
           'nama' => $nama,
           'tempat_lahir' => $tempat_lahir,
           'tgl_lahir' => $tgl_lahir,
           'jk' => $jk,
           'alamat' => $alamat,
           'email' => $email,
           'status' => $status,
           'no_sim' => $no_sim,
           'jabatan' => $jabatan,
           'password' => password_hash($password, PASSWORD_DEFAULT),
           'foto' => $image2,
         );
           $result['pesan']="";
           $this->Karyawan_model->ubah($data,$id);
       }
     }
     echo json_encode($result);
   }
   public function getJabatan()
   {
     $data = $this->Jabatan_model->view();
     echo json_encode($data);
   }
   public function acakpassword()
   {
     $panjang = 6;
     $karakter= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
     $string = '';
     for ($i = 0; $i < $panjang; $i++) {
       $pos = rand(0, strlen($karakter)-1);
       $string .= $karakter{$pos};
    }
    return $string;
   }

   public function download()
   {
     $karyawan = $this->Karyawan_model->cetak();
     $this->load->library('pdf');
     $this->pdf->set_option('isRemoteEnabled', TRUE);
     $this->pdf->setPaper('F4', 'landscape');
     $this->pdf->filename = "laporan-data-karyawan.pdf";
     $this->pdf->load_view('karyawan/cetak', compact('karyawan'));
   }
}
