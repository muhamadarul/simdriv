<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Laporan_perjalanan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('Auth_model');
        $this->load->model('Perjalanan_model');
        $this->load->model('Pengeluaran_model');

        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
        }
    }
    public function index()
    {
      $data['title'] = "Beranda";
      $data['user'] = $this->Auth_model->success_login();
      $this->load->view('templates/header',$data);
      $this->load->view('l_perjalanan/index',$data);
      $this->load->view('templates/footer');
    }
    public function cetakByTanggal()
    {
      $tgl_awal = $this->input->post('tgl_awal');
      $tgl_akhir = $this->input->post('tgl_akhir');
      $perjalanan = $this->Perjalanan_model->cetakByTanggal($tgl_awal,$tgl_akhir);
      $pengeluaran = $this->Pengeluaran_model->viewAll();
      $this->load->library('pdf');
      $this->pdf->set_option('isRemoteEnabled', TRUE);
      $this->pdf->setPaper('A3', 'landscape');
      $this->pdf->filename = "laporan-data-perjalanan.pdf";
      $this->pdf->load_view('l_perjalanan/cetak_pdf', compact('perjalanan','pengeluaran'));
    }
    public function tes()
    {
      $perjalanan = $this->Perjalanan_model->tes();
      print_r($perjalanan);
    }
}
