<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Profile extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Karyawan_model');
    		if (!$this->session->userdata('email')) {
    						$this->session->set_flashdata('error', 'You Are Not Logged In!');
    						redirect('login');
    		}
    }
    public function index()
    {
      $data['title'] = "Your Profile";
      $data['karyawan'] = $this->Auth_model->success_login_karyawan();
      $id = $data['karyawan']['id'];
      $data['detail_karyawan'] = $this->Karyawan_model->getDetail($id);
      $this->load->view('templates/k_header',$data);
      $this->load->view('v_karyawan/profil/index');
      $this->load->view('templates/k_footer');
    }
}
