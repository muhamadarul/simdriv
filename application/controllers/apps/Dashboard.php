<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Dashboard extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Beranda_model');
    		if (!$this->session->userdata('email')) {
    						$this->session->set_flashdata('error', 'You Are Not Logged In!');
    						redirect('login');
    		}
    }
    public function index()
    {
      $data['title'] = "Dashboard";
      $data['karyawan'] = $this->Auth_model->success_login_karyawan();
      $id = $data['karyawan']['id'];
      $data['approved'] = $this->Beranda_model->countApproved($id);
      $data['rejected'] = $this->Beranda_model->countRejected($id);
      $this->load->view('templates/k_header',$data);
      $this->load->view('v_karyawan/dashboard/index',$data);
      $this->load->view('templates/k_footer');
    }
}
