<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Application extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Perjalanan_model');
        $this->load->model('Pengajuan_model');
    		if (!$this->session->userdata('email')) {
    						$this->session->set_flashdata('error', 'You Are Not Logged In!');
    						redirect('login');
    		}
    }
    public function form_application()
    {
      $data['title'] = "Application";
      $data['karyawan'] = $this->Auth_model->success_login_karyawan();
      $this->load->view('templates/k_header',$data);
      $this->load->view('v_karyawan/app/index');
      $this->load->view('templates/k_footer');
    }
    public function add()
    {
      $destination = $this->input->post('destination');
      $id = $this->input->post('id');
      $date_of_departure = $this->input->post('date_of_departure');
      $reason = $this->input->post('reason');
      $date_of_filling = date('y-m-d');
      if ($destination=="") {
        $result['message'] ="Please fill out your destination";
      }elseif ($date_of_departure=="") {
        $result['message'] ="Please fill out your departure date";
      }elseif ($reason=="") {
        $result['message'] ="Please fill out your reason";
      }else {
        $result['message'] ="";
        $data = $this->Pengajuan_model->select_max();
        $code = $data['code'];
        $order = (int) substr($code,6,6);
        $no=$order+1;
        $alphabet = "APP";
        $app_number = $alphabet . sprintf("%05s", $no);
        $data =[
          'no_pengajuan'=>$app_number,
          'karyawan'=>$id,
          'tgl_pengajuan'=>$date_of_filling,
          'tujuan'=>$destination,
          'tgl_berangkat'=>$date_of_departure,
          'status'=> 2,
          'keperluan'=>$reason,
        ];
        $this->Pengajuan_model->tambah($data);
      }
      echo json_encode($result);
    }
    public function tes()
    {
      $date_of_filling = $this->Pengajuan_model->select_max();
      var_dump($date_of_filling);
    }
    public function data()
    {
      $data['title'] = "Your Application";
      $data['karyawan'] = $this->Auth_model->success_login_karyawan();
      $id = $data['karyawan']['id'];
      $data['app'] = $this->Pengajuan_model->getAllById($id);
      $this->load->view('templates/k_header',$data);
      $this->load->view('v_karyawan/app/data',$data);
      $this->load->view('templates/k_footer');
    }
    public function travel_history()
    {
      $data['title'] = "Your Travel History";
      $data['karyawan'] = $this->Auth_model->success_login_karyawan();
      $id = $data['karyawan']['id'];
      $data['travel'] = $this->Perjalanan_model->getAllById($id);
      $this->load->view('templates/k_header',$data);
      $this->load->view('v_karyawan/app/travel',$data);
      $this->load->view('templates/k_footer');
    }
}
