<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Change_password extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Karyawan_model');
    		if (!$this->session->userdata('email')) {
    						$this->session->set_flashdata('error', 'You Are Not Logged In!');
    						redirect('login');
    		}
    }
    public function index()
    {
      $data['title'] = "Change Your Password";
      $data['karyawan'] = $this->Auth_model->success_login_karyawan();
      $id=   $data['karyawan']['id'];
      $this->load->view('templates/k_header',$data);
      $this->load->view('v_karyawan/ubah_pass/index',$data);
      $this->load->view('templates/k_footer');
    }
    public function action()
    {
      $data['karyawan'] = $this->Auth_model->success_login_karyawan();
      $data_old_pass = $data['karyawan']['password'];
      $id = $this->input->post('id');
      $old_pass = $this->input->post('password_lama');
      $new_pass = $this->input->post('password_baru');
      $con_pass = $this->input->post('password_baru2');
      $hash_old =  password_hash($old_pass, PASSWORD_DEFAULT);
      $hash_new =  password_hash($new_pass, PASSWORD_DEFAULT);
      $hash_con =  password_hash($con_pass, PASSWORD_DEFAULT);
      if ($old_pass=="") {
        $result['message'] = "Please fill your old password";
      }elseif ($new_pass=="") {
        $result['message'] = "Please fill your new password";
      }elseif ($con_pass=="") {
        $result['message'] = "Please confirm your new password";
      }elseif (!password_verify($old_pass,$data_old_pass)) {
        $result['message'] = "Wrong Old Password";
      }elseif (password_verify($new_pass,$data_old_pass)) {
        $result['message'] = "The new password cannot be the same as the old password";
      }elseif ($new_pass!=$con_pass) {
        $result['message'] = "New password not confirmed";
      }else {
        $result['message'] ="";
        $this->Karyawan_model->ubah_pass($id,$hash_con);
      }
      echo json_encode($result);

    }
    public function tes()
    {
      $data['karyawan'] = $this->Auth_model->success_login_karyawan();
      $data_old_pass = [
        $data['karyawan']['password'],
        password_hash('desta', PASSWORD_DEFAULT)
      ];
      var_dump($data_old_pass);
    }

}
