<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jabatan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('Jabatan_model');
        $this->load->model('Auth_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
        }
    }
    public function index()
    {
      $data['title'] = "Jabatan";
      $data['user'] = $this->Auth_model->success_login();
      $this->load->view('templates/header',$data);
      $this->load->view('jabatan/index');
      $this->load->view('templates/footer');
    }
    public function ajax_list()
    {
        $list = $this->Jabatan_model->get_datatables();
        $data = array();
        if(isset($_POST['start']) && isset($_POST['draw']))
        {
          $no = $_POST['start'];
        }else{
             // need start and draw
             die(); // some error
        }
        foreach ($list as $jabatan) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $jabatan->nama_jabatan;
            $row[] = '<a class="btn btn-default btn-outline-success btn-sm" href="javascript:void(0)" title="Edit" onclick="edit('."'".$jabatan->id."'".')"><i class="fa fa-edit"></i></a>
                      <a class="btn btn-default btn-outline-danger btn-sm" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$jabatan->id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Jabatan_model->count_all(),
                        "recordsFiltered" => $this->Jabatan_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function tambah()
    {
      $nama_jabatan = $this->input->post('nama_jabatan');
      if ($nama_jabatan=='') {
        $result['pesan'] ="Nama Jabatan Harus Diisi";
      }else {
        $result['pesan']="";
        $data = array(
                'nama_jabatan' => $nama_jabatan,
              );
          $this->Jabatan_model->tambah($data);
      }
      echo json_encode($result);
   }
   public function hapus()
   {
     $id = $this->input->post('id');
     $this->Jabatan_model->hapus($id);
   }
   public function ajax_edit($id)
   {
     $data = $this->Jabatan_model->getById($id);
     echo json_encode($data);
   }
   public function ubah()
   {
     $id = $this->input->post('id');
     $nama_jabatan = $this->input->post('nama_jabatan2');
     if ($nama_jabatan=='') {
       $result['pesan'] ="Nama Jabatan Harus Diisi";
     }else {
       $result['pesan']="";
       $data = array(
               'nama_jabatan' => $nama_jabatan,
             );
         $this->Jabatan_model->ubah($id,$data);

     }
     echo json_encode($result);
   }


   public function download()
   {
     $jabatan = $this->Jabatan_model->cetak();
     $this->load->library('pdf');
     $this->pdf->set_option('isRemoteEnabled', TRUE);
     $this->pdf->setPaper('F4', 'landscape');
     $this->pdf->filename = "laporan-data-jabatan.pdf";
     $this->pdf->load_view('jabatan/cetak', compact('jabatan'));
   }
}
