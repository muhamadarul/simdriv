<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Auth_model');
    $this->load->helper('url');
	}

	public function index()
	{
		$this->form_validation->set_rules('email', 'Email', 'required', [
			'required' => 'Email tidak boleh kosong!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|trim', [
			'required' => 'Password tidak boleh kosong'
		]);

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Login';
			$this->load->view('templates/login_header',$data);
			$this->load->view('login/index',$data);
		} else {
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$user = $this->Auth_model->login_karyawan($email, $password);

			if ($user) {
				//jika usernya sudah aktif
				if ($user['status'] == 1) {
					//cek password
					if (password_verify($password, $user['password'])) {
						$data = [
							'email' => $user['email'],
              'jabatan' => $user['jabatan']
						];
						$this->session->set_userdata($data);
						$this->session->set_flashdata('success', "You've been logged in");
						redirect('apps/dashboard');
					} else {
						$this->session->set_flashdata('warning', 'Wrong Password, Check Again !');
						redirect('login');
					}
				} else {
					$this->session->set_flashdata('warning', 'Email not Activated');
					redirect('login');
				}
			} else {
				$this->session->set_flashdata('error', 'Email not Registered!');
				redirect('login');
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('jabatan');
		$this->session->set_flashdata('success', "You've been logged out");
		redirect('login');
	}
}
