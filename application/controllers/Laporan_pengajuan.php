<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Laporan_pengajuan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('Auth_model');
        $this->load->model('Pengajuan_model');
    		if (!$this->session->userdata('username')) {
    						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
    						redirect('auth');
    		}
    }
    public function index()
    {
      $data['title'] = "Beranda";
      $data['user'] = $this->Auth_model->success_login();
      $this->load->view('templates/header',$data);
      $this->load->view('l_pengajuan/index',$data);
      $this->load->view('templates/footer');
    }
    public function cetakByTanggal()
    {
      $tgl_awal = $this->input->post('tgl_awal');
      $tgl_akhir = $this->input->post('tgl_akhir');
      $pengajuan = $this->Pengajuan_model->cetakByTanggal($tgl_awal,$tgl_akhir);
      $this->load->library('pdf');
  		$this->pdf->set_option('isRemoteEnabled', TRUE);
      $this->pdf->setPaper('A3', 'landscape');
      $this->pdf->filename = "laporan-data-pengajuan.pdf";
      $this->pdf->load_view('l_pengajuan/cetak_pdf', compact('pengajuan'));
    }
    public function tes()
    {
      $tgl_awal = '2021-07-1';
      $tgl_akhir ='2021-07-10';
      $pengajuan = $this->Pengajuan_model->cetakByTanggal($tgl_awal,$tgl_akhir);
      var_dump($pengajuan);
    }
}
