<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perjalanan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('Perjalanan_model');
        $this->load->model('Pengeluaran_model');
        $this->load->model('Auth_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
        }
    }
    public function index()
    {
      $data['title'] = "Perjalanan Kendaraan";
      $data['user'] = $this->Auth_model->success_login();
      $this->load->view('templates/header',$data);
      $this->load->view('perjalanan/index');
      $this->load->view('templates/footer');
    }
    public function ajax_list()
    {
        $list = $this->Perjalanan_model->get_datatables();
        $data = array();
        if(isset($_POST['start']) && isset($_POST['draw']))
        {
          $no = $_POST['start'];
        }else{
             // need start and draw
             die(); // some error
        }
        foreach ($list as $perjalanan) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $perjalanan->no_plat;
            $row[] = $perjalanan->merk;
            $row[] = $perjalanan->no_pengajuan;
            $row[] = $perjalanan->tgl_pengajuan;
            $row[] = $perjalanan->nama;
            $row[] = $perjalanan->nama_driver;
            $row[] = $perjalanan->tgl_berangkat;
            $row[] = $perjalanan->keperluan;
            $row[] = $perjalanan->tujuan;
            if ($perjalanan->status_perjalanan == 3) {
              $status = '<span class="label label-warning">Pending</span>';
            }elseif ($perjalanan->status_perjalanan == 1) {
              $status = '<span class="label label-success">Selesai</span>';
            }elseif ($perjalanan->status_perjalanan == 2) {
              $status = '<span class="label label-primary">Sedang Berlangsung</span>';
            }else {
              $status = '<span class="label label-danger">Dibatalkan</span>';
            }
            $row[] = $status;
            if ($perjalanan->status_perjalanan == 1) {
              $row[] = '<a class="btn btn-default btn-outline-success btn-sm" href="javascript:void(0)" title="Detail Pengeluaran" onclick="detail('."'".$perjalanan->id_perjalanan."'".')"><i class="fa fa-money"></i></a>';
            }

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Perjalanan_model->count_all(),
                        "recordsFiltered" => $this->Perjalanan_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

   public function ajax_edit($id)
   {
     $data = $this->Perjalanan_model->getById($id);
     echo json_encode($data);
   }
   public function tambah()
   {
     $pengajuan = $this->input->post('pengajuan');
     $driver = $this->input->post('driver');
     $vehicle = $this->input->post('kendaraan');
     $cek = $this->Perjalanan_model->checkExist($pengajuan);
     $status = 3;
     if ($cek > 0) {
       $result['pesan'] = "Perjalanan Sudah Dibuat untuk Pengajuan ini";
     }elseif ($driver=='') {
       $result['pesan'] = "Driver harus dipilih";
     }elseif ($vehicle=='') {
       $result['pesan'] = "Vehicle harus dipilih";
     }else {
       $result['pesan'] ="";
            $data = [
              'status' => $status,
              'pengajuan' => $pengajuan,
              'driver' => $driver,
              'vehicle' => $vehicle,
            ];
            $this->Perjalanan_model->tambah($data);
     }
     echo json_encode($result);
   }
   public function getPengeluaran()
   {
    $id =$this->input->post('id');
    $data = $this->Pengeluaran_model->view($id);
    echo json_encode($data);
   }
   public function getDetailPerjalanan()
   {
     $id =$this->input->post('id');
     $data2 = $this->Perjalanan_model->getDetail($id);
     echo json_encode($data2);
   }
   public function tes()
   {
     $id =1;
     $data2 = $this->Perjalanan_model->getDetail($id);
     var_dump($data2);
   }
}
