<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Profil extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('User_model');
        $this->load->model('Auth_model');
    		if (!$this->session->userdata('username')) {
    						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
    						redirect('auth');
    		}
    }
    public function index()
    {
      $data['title'] = "Profil";
      $data['user'] = $this->Auth_model->success_login();
      $id =   $data['user']['id'];
      $data['detail']= $this->User_model->getDetail($id);
      $this->form_validation->set_rules('password_lama', 'Password Lama', 'required|trim', array(
          'required' => 'Password Lama harus diisi!'
      ));

      $this->form_validation->set_rules('password_baru1', 'Password Baru', 'required|trim|min_length[5]|matches[password_baru2]', array(
          'required' => 'Password baru tidak boleh kosong!',
          'matches' => 'Password tidak sesuai!',
          'min_length' => 'Password terlalu pendek!',
      ));

      $this->form_validation->set_rules('password_baru2', 'Password Baru', 'required|trim|min_length[5]|matches[password_baru1]', array(
          'required' => 'Password baru tidak boleh kosong!',
          'matches' => 'Password tidak sesuai!',
          'min_length' => 'Password terlalu pendek!',
      ));
      if ($this->form_validation->run() == false) {
        $this->load->view('templates/header',$data);
        $this->load->view('profil/index',$data);
        $this->load->view('templates/footer');
      }else {
        $password_lama = password_hash($this->input->post('password_lama'), PASSWORD_DEFAULT);
          $password_baru = $this->input->post('password_baru1');
            if (!password_verify($password_lama, $data['user']['password'])) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger role="alert">Password lama salah!</div>');
            redirect('profil');
            } else {
              if ($password_lama==$password_baru) {
                $this->session->set_flashdata('message',  '<div class="alert alert-danger role="alert">Password tidak boleh sama</div>');
                redirect('profil');
              }else{
                $password_hash = password_hash($password_baru, PASSWORD_DEFAULT);
                $this->db->set('password', $password_hash);
                $this->db->where('id', $this->input->post('id'));
                $this->db->update('user');
                $this->session->set_flashdata('success', 'Password berhasil diubah!');
                redirect('beranda');
              }
           }
      }

    }
}
