<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('auth');
		}
	}

	public function index()
	{
		$data['title'] = "User";
		$data['user'] = $this->Auth_model->success_login();
    $this->load->view('templates/header',$data);
    $this->load->view('user/index');
    $this->load->view('templates/footer');
	}
	public function getAkses()
	{
	$data = $this->User_model->getAkses();
	echo json_encode($data);

	}
	public function view()
	{
		$data = $this->User_model->view();
		echo json_encode($data);
	}
	public function tambahuser()
	{
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$status = $this->input->post('status');
		$level = $this->input->post('level');
		$password1 = $this->input->post('password1');
		$password2 = $this->input->post('password2');
		$cekUsername = $this->User_model->cekUsername($username);
		if ($nama == '') {
		  $result['pesan'] ="Nama Harus Diisi";
		}elseif ($username=='') {
		  $result['pesan'] ="Username Harus Diisi";
		}elseif ($password1 =='') {
			$result['pesan'] ="Password Harus Diisi";
		}elseif ($password2 =='') {
			$result['pesan'] = "Konfirmasi Password";
		}elseif($status =='') {
			$result['pesan'] ="Status Harus Dipilih";
		}elseif($level =='') {
			$result['pesan'] ="Level Harus Dipilih";
		}elseif ($password1 != $password2) {
			$result['pesan'] ="Password Tidak Sesuai";
		}elseif (strlen($password1) < 5) {
	  	$result['pesan'] ="Password Minimal 5 Karakter";
		}elseif ($username == $cekUsername['username']){
			$result['pesan'] ="Username Sudah Digunakan";
		}else {
			$result['pesan'] ="";
			$data = [
			    'nama' => htmlspecialchars($nama),
			    'username' => htmlspecialchars($username),
			    'password' => password_hash($password1, PASSWORD_DEFAULT),
			    'status' => htmlspecialchars($status),
					'level' => htmlspecialchars($level),
			 ];
			$this->User_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->User_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('e_user');
		$nama = $this->input->post('e_nama');
		$status = $this->input->post('e_status');
		$level = $this->input->post('e_level');
		if ($nama == '') {
		  $result['pesan'] ="Nama Harus Diisi";
		}elseif($status =='') {
			$result['pesan'] ="Status Harus Dipilih";
		}else {
			$result['pesan'] ="";
			$data = [
			    'nama' => htmlspecialchars($nama),
			    'status' => htmlspecialchars($status),
					'level' => htmlspecialchars($level),
			 ];
			$this->User_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->User_model->hapus($id);
	}

}
