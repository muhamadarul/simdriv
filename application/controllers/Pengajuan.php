<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengajuan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('Pengajuan_model');
        $this->load->model('Karyawan_model');
        $this->load->model('Vehicle_model');
        $this->load->model('Driver_model');
        $this->load->model('Auth_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
        }
    }
    public function index()
    {
      $data['title'] = "Pengajuan Kendaraan";
      $data['user'] = $this->Auth_model->success_login();
      $this->load->view('templates/header',$data);
      $this->load->view('pengajuan/index');
      $this->load->view('templates/footer');
    }
    public function ajax_list()
    {
        $list = $this->Pengajuan_model->get_datatables();
        $data = array();
        if(isset($_POST['start']) && isset($_POST['draw']))
        {
          $no = $_POST['start'];
        }else{
             // need start and draw
             die(); // some error
        }
        foreach ($list as $pengajuan) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pengajuan->no_pengajuan;
            $row[] = $pengajuan->tgl_pengajuan;
            $row[] = $pengajuan->nama;
            $row[] = $pengajuan->tgl_berangkat;
            $row[] = $pengajuan->keperluan;
            if ($pengajuan->status_p == 2) {
              $status = '<span class="label label-warning">Pending</span>';
            }elseif ($pengajuan->status_p == 1) {
              $status = '<span class="label label-success">Approved</span>';
            }else {
              $status = '<span class="label label-danger">Rejected</span>';
            }
            $row[] = $status;
            //add html for action
            if ($pengajuan->status_p == 1) {
              $row[] = '<a class="btn btn-default btn-outline-success btn-sm" href="javascript:void(0)" title="Edit" onclick="edit('."'".$pengajuan->id_pengajuan."'".')"><i class="fa fa-edit"></i></a>
              <a class="btn btn-default btn-outline-warning btn-sm" href="javascript:void(0)" title="Buat Perjalanan" onclick="perjalanan('."'".$pengajuan->id_pengajuan."'".')"><i class="fa fa-car"></i></a>';
            }else {
              $row[] = '<a class="btn btn-default btn-outline-success btn-sm" href="javascript:void(0)" title="Edit" onclick="edit('."'".$pengajuan->id_pengajuan."'".')"><i class="fa fa-edit"></i></a>';
            }

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Pengajuan_model->count_all(),
                        "recordsFiltered" => $this->Pengajuan_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

   public function ajax_edit($id)
   {
     $data = $this->Pengajuan_model->getById($id);
     echo json_encode($data);
   }
   public function ubah()
   {
     $id = $this->input->post('id');
     $status = $this->input->post('status');
     $data = [
       'status' => $status,
     ];
     $this->Pengajuan_model->ubah($id,$data);
     $result['pesan'] = "";
     echo json_encode($result);
   }
   public function getDriver()
   {
     $nama= $this->input->get('driver');
     $query = $this->Driver_model->getDriver($nama,'nama');
     echo json_encode($query);
   }
   public function getKendaraan()
   {
     $no_plat= $this->input->get('kendaraan');
     $query = $this->Vehicle_model->getKendaraan($no_plat,'no_plat');
     echo json_encode($query);
   }
}
