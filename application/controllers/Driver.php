<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Driver extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('Driver_model');
        $this->load->model('Auth_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
        }
    }
    public function index()
    {
      $data['title'] = "Driver";
      $data['user'] = $this->Auth_model->success_login();
      $this->load->view('templates/header',$data);
      $this->load->view('driver/index');
      $this->load->view('templates/footer');
    }
    public function ajax_list()
    {
        $list = $this->Driver_model->get_datatables();
        $data = array();
        if(isset($_POST['start']) && isset($_POST['draw']))
        {
          $no = $_POST['start'];
        }else{
             // need start and draw
             die(); // some error
        }
        foreach ($list as $driver) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $driver->nama;
            $row[] = $driver->email;
            $row[] = $driver->no_sim;
            $row[] = $driver->jk;
            $row[] = $driver->tempat_lahir;
            $row[] = $driver->tgl_lahir;
            if ($driver->status == 1) {
              $status = '<span class="label label-success">Active</span>';
            }else {
              $status = '<span class="label label-danger">Deactive</span>';
            }
            $row[] = $status;
            //add html for action
            $row[] = '<a class="btn btn-default btn-outline-success btn-sm" href="javascript:void(0)" title="Edit" onclick="edit('."'".$driver->id."'".')"><i class="fa fa-edit"></i></a>
                      <a class="btn btn-default btn-outline-warning btn-sm" href="javascript:void(0)" title="Detail" onclick="detail('."'".$driver->id."'".')"><i class="fa fa-eye"></i></a>
                      <a class="btn btn-default btn-outline-danger btn-sm" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$driver->id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Driver_model->count_all(),
                        "recordsFiltered" => $this->Driver_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    private function _uploadImage()
    {
      //upload foto
      $config['upload_path']          = './assets/img/driver';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['encrypt_name'] = TRUE; //enkripsi file name upload
      $config['overwrite']            = true;
      $config['max_size']             = 520; // 500kb
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;
      $this->load->library('upload', $config);
      if ($this->upload->do_upload('foto')) {
          $a = array('upload_data' => $this->upload->data());
          $image= $a['upload_data']['file_name'];
          // $gambar = $_FILES['userfile']['name'];
      }else {
          $image= 'default.jpg';
          $foto = $this->upload->display_errors();
      }
    }
    public function tambah()
    {
      $nama = $this->input->post('nama');
      $tempat_lahir = $this->input->post('tempat_lahir');
      $tgl_lahir = $this->input->post('tgl_lahir');
      $jk  = $this->input->post('jk');
      $alamat = $this->input->post('alamat');
      $email = $this->input->post('email');
      $status = $this->input->post('status');
      $password  = $this->input->post('password');
      $no_sim  = $this->input->post('no_sim');
      //upload foto
      $config['upload_path']          = './assets/img/driver';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['encrypt_name']         = TRUE; //enkripsi file name upload
      $config['overwrite']            = true;
      $config['max_size']             = 520; // 500kb
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;
      $this->load->library('upload', $config);
      if ($this->upload->do_upload('foto')) {
          $a = array('upload_data' => $this->upload->data());
          $image= $a['upload_data']['file_name'];
          // $gambar = $_FILES['userfile']['name'];
      }else {
          $image= 'default.jpg';
      }
      $error_upload = $this->upload->display_errors();
      if ($nama=='') {
        $result['pesan'] ="Nama Harus Diisi";
      }elseif ($tempat_lahir=='') {
        $result['pesan'] ="Tempat Lahir Harus Diisi";
      }elseif ($tgl_lahir=='') {
        $result['pesan'] ="Tanggal Lahir Harus Diisi";
      }elseif ($jk=='') {
        $result['pesan'] ="JK Harus Diisi";
      }elseif ($alamat=='') {
        $result['pesan'] ="Alamat Harus Diisi";
      }elseif ($email=='') {
        $result['pesan'] ="Email Harus Diisi";
      }elseif ($status=='') {
        $result['pesan'] ="Status Harus Diisi";
      }elseif ($no_sim=='') {
        $result['pesan'] ="No sim Harus Diisi";
      }elseif($error_upload !='') {
        $result['pesan'] = $error_upload;
      }else {
        $result['pesan']="";
        $data = array(
                'nama' => $nama,
                'tempat_lahir' => $tempat_lahir,
                'tgl_lahir' => $tgl_lahir,
                'jk' => $jk,
                'alamat' => $alamat,
                'foto' => $image,
                'status' => $status,
                'no_sim' => $no_sim,
                'email' => $email,
                'password' => password_hash($password, PASSWORD_DEFAULT),

              );
          $this->Driver_model->tambah($data);
      }
      echo json_encode($result);
   }
   public function hapus()
   {
     $id = $this->input->post('id');
     $this->Driver_model->hapus($id);
   }
   public function ajax_edit($id)
   {
     $data = $this->Driver_model->getById($id);
     echo json_encode($data);
   }
   public function ubah()
   {
     $id = $this->input->post('id');
     $nama = $this->input->post('nama2');
     $tempat_lahir = $this->input->post('tempat_lahir2');
     $tgl_lahir = $this->input->post('tgl_lahir2');
     $jk  = $this->input->post('jk2');
     $alamat = $this->input->post('alamat2');
     $email = $this->input->post('email2');
     $status = $this->input->post('status2');
     $password  = $this->input->post('password2');
     $no_sim  = $this->input->post('no_sim2');
     $image2  = $this->input->post('foto_lama');

     if ($nama=='') {
       $result['pesan'] ="Nama Harus Diisi";
     }elseif ($tempat_lahir=='') {
       $result['pesan'] ="Tempat Lahir Harus Diisi";
     }elseif ($tgl_lahir=='') {
       $result['pesan'] ="Tanggal Lahir Harus Diisi";
     }elseif ($jk=='') {
       $result['pesan'] ="JK Harus Diisi";
     }elseif ($alamat=='') {
       $result['pesan'] ="Alamat Harus Diisi";
     }elseif ($email=='') {
       $result['pesan'] ="Email Harus Diisi";
     }elseif ($status=='') {
       $result['pesan'] ="Status Harus Diisi";
     }elseif ($no_sim=='') {
       $result['pesan'] ="No sim Harus Diisi";
     }else {
       if ($password=='' && (empty($_FILES['foto2']['name']))) {
         $data = array(
                 'nama' => $nama,
                 'tempat_lahir' => $tempat_lahir,
                 'tgl_lahir' => $tgl_lahir,
                 'jk' => $jk,
                 'alamat' => $alamat,
                 'email' => $email,
                 'status' => $status,
                 'no_sim' => $no_sim,
                 'foto' => $image2,
               );
           $result['pesan']="";
           $this->Driver_model->ubah($data,$id);
       }elseif ($password !='' && (!empty($_FILES['foto2']['name']))) {
         //upload foto
         $config['upload_path']          = './assets/img/driver';
         $config['allowed_types']        = 'gif|jpg|png|jpeg';
         $config['encrypt_name']         = TRUE; //enkripsi file name upload
         $config['overwrite']            = true;
         $config['max_size']             = 520; // 500kb
         // $config['max_width']            = 1024;
         // $config['max_height']           = 768;
         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto2')) {
             $a = array('upload_data' => $this->upload->data());
             $image= $a['upload_data']['file_name'];
             // $gambar = $_FILES['userfile']['name'];
         }else {
             $image= 'default.jpg';
         }
         $error_upload = $this->upload->display_errors();
         //jika upload gagal
           if($error_upload !='') {
           $result['pesan'] = $error_upload;
           //upload berhasil
           }else {
             $data = array(
                 'nama' => $nama,
                 'tempat_lahir' => $tempat_lahir,
                 'tgl_lahir' => $tgl_lahir,
                 'jk' => $jk,
                 'alamat' => $alamat,
                 'email' => $email,
                 'status' => $status,
                 'no_sim' => $no_sim,
                 'password' => password_hash($password, PASSWORD_DEFAULT),
                 'foto' => $image,
               );
             $result['pesan']="";
             $this->Driver_model->ubah($data,$id);
           }
       }elseif ($password =='' && (!empty($_FILES['foto2']['name']))) {
         //upload foto
         $config['upload_path']          = './assets/img/driver';
         $config['allowed_types']        = 'gif|jpg|png|jpeg';
         $config['encrypt_name']         = TRUE; //enkripsi file name upload
         $config['overwrite']            = true;
         $config['max_size']             = 520; // 500kb
         // $config['max_width']            = 1024;
         // $config['max_height']           = 768;
         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto2')) {
             $a = array('upload_data' => $this->upload->data());
             $image= $a['upload_data']['file_name'];
             // $gambar = $_FILES['userfile']['name'];
         }else {
             $image= 'default.jpg';
         }
         $error_upload = $this->upload->display_errors();
         //jika upload gagal
           if($error_upload !='') {
           $result['pesan'] = $error_upload;
           //upload berhasil
           }else {
             $data = array(
               'nama' => $nama,
               'tempat_lahir' => $tempat_lahir,
               'tgl_lahir' => $tgl_lahir,
               'jk' => $jk,
               'alamat' => $alamat,
               'email' => $email,
               'status' => $status,
               'no_sim' => $no_sim,
               'foto' => $image,
               );
             $result['pesan']="";
             $this->Driver_model->ubah($data,$id);
           }
       }else {
         $data = array(
           'nama' => $nama,
           'tempat_lahir' => $tempat_lahir,
           'tgl_lahir' => $tgl_lahir,
           'jk' => $jk,
           'alamat' => $alamat,
           'email' => $email,
           'status' => $status,
           'no_sim' => $no_sim,
           'password' => password_hash($password, PASSWORD_DEFAULT),
           'foto' => $image2,
         );
           $result['pesan']="";
           $this->Driver_model->ubah($data,$id);
       }
     }
     echo json_encode($result);
   }
   public function download()
   {
     $driver = $this->Driver_model->cetak();
     $this->load->library('pdf');
     $this->pdf->set_option('isRemoteEnabled', TRUE);
     $this->pdf->setPaper('F4', 'landscape');
     $this->pdf->filename = "laporan-data-driver.pdf";
     $this->pdf->load_view('driver/cetak', compact('driver'));
   }
}
