<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Auth_model');
    $this->load->helper('url');
	}

	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'required', [
			'required' => 'Username tidak boleh kosong!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|trim', [
			'required' => 'Password tidak boleh kosong'
		]);

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Login Admin';
			$this->load->view('templates/auth_header',$data);
			$this->load->view('auth/index',$data);
			$this->load->view('templates/auth_footer');
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$user = $this->Auth_model->login($username, $password);

			if ($user) {
				//jika usernya sudah aktif
				if ($user['status'] == 1) {
					//cek password
					if (password_verify($password, $user['password'])) {
						$data = [
							'username' => $user['username'],
              'id_akses' => $user['level']
						];
						$this->session->set_userdata($data);
						$this->session->set_flashdata('success', 'Login Berhasil');
						redirect('beranda');
					} else {
						$this->session->set_flashdata('warning', 'Password salah, periksa kembali');
						redirect('auth');
					}
				} else {
					$this->session->set_flashdata('warning', 'Username belum diaktivasi');
					redirect('auth');
				}
			} else {
				$this->session->set_flashdata('error', 'Username belum terdaftar!');
				redirect('auth');
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('id_akses');
		$this->session->set_flashdata('success', 'Anda telah logout');
		redirect('auth');
	}


}
