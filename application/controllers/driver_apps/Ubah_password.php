<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Ubah_password extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Driver_model');
    		if (!$this->session->userdata('email_driver')) {
    						$this->session->set_flashdata('error', 'You Are Not Logged In!');
    						redirect('driver_apps/login');
    		}
    }
    public function index()
    {
      $data['title'] = "Ubah Password";
      $data['driver'] = $this->Auth_model->success_login_driver();
      $id=   $data['driver']['id'];
      $this->load->view('v_driver/templates/header',$data);
      $this->load->view('v_driver/ubah_pass/index',$data);
      $this->load->view('v_driver/templates/footer');
    }
    public function action()
    {
      $data['driver'] = $this->Auth_model->success_login_driver();
      $data_old_pass = $data['driver']['password'];
      $id = $this->input->post('id');
      $old_pass = $this->input->post('password_lama');
      $new_pass = $this->input->post('password_baru');
      $con_pass = $this->input->post('password_baru2');
      $hash_old =  password_hash($old_pass, PASSWORD_DEFAULT);
      $hash_new =  password_hash($new_pass, PASSWORD_DEFAULT);
      $hash_con =  password_hash($con_pass, PASSWORD_DEFAULT);
      if ($old_pass=="") {
        $result['message'] = "Please fill your old password";
      }elseif ($new_pass=="") {
        $result['message'] = "Please fill your new password";
      }elseif ($con_pass=="") {
        $result['message'] = "Please confirm your new password";
      }elseif (!password_verify($old_pass,$data_old_pass)) {
        $result['message'] = "Wrong Old Password";
      }elseif (password_verify($new_pass,$data_old_pass)) {
        $result['message'] = "The new password cannot be the same as the old password";
      }elseif ($new_pass!=$con_pass) {
        $result['message'] = "New password not confirmed";
      }else {
        $result['message'] ="";
        $this->Driver_model->ubah_pass($id,$hash_con);
      }
      echo json_encode($result);

    }


}
