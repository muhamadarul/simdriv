<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Riwayat_tugas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Perjalanan_model');
        $this->load->model('Pengajuan_model');
        if (!$this->session->userdata('email_driver')) {
    						$this->session->set_flashdata('error', 'You Are Not Logged In!');
    						redirect('driver_apps/login');
    		}
    }

    public function index()
    {
      $data['title'] = "Histori Perjalanan";
      $data['driver'] = $this->Auth_model->success_login_driver();
      $id = $data['driver']['id'];
      $data['travel'] = $this->Perjalanan_model->getAllByIdDriver($id);
      $this->load->view('v_driver/templates/header',$data);
      $this->load->view('v_driver/riwayat_tugas/index',$data);
      $this->load->view('v_driver/templates/footer');
    }
}
