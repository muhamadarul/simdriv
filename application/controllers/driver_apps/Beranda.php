<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Beranda extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Beranda_model');
    		if (!$this->session->userdata('email_driver')) {
    						$this->session->set_flashdata('error', 'You Are Not Logged In!');
    						redirect('driver_apps/login');
    		}
    }
    public function index()
    {
      $data['title'] = "Dashboard";
      $data['driver'] = $this->Auth_model->success_login_driver();
      $this->load->view('v_driver/templates/header',$data);
      $this->load->view('v_driver/beranda/index',$data);
      $this->load->view('v_driver/templates/footer');
    }
}
