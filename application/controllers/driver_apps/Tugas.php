<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Tugas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Perjalanan_model');
        $this->load->model('Pengeluaran_model');
        $this->load->model('Pengajuan_model');
        if (!$this->session->userdata('email_driver')) {
    						$this->session->set_flashdata('error', 'You Are Not Logged In!');
    						redirect('driver_apps/login');
    		}
    }

    public function index()
    {
      $data['title'] = "Tugas Hari Ini";
      $data['driver'] = $this->Auth_model->success_login_driver();
      $id = $data['driver']['id'];
      $today = date('y-m-d');
      $data['tugas'] =$this->Perjalanan_model->getTodayDriver($today,$id);
      $this->load->view('v_driver/templates/header',$data);
      $this->load->view('v_driver/tugas/index',$data);
      $this->load->view('v_driver/templates/footer');
    }
    public function berangkat()
    {
      $id = $this->input->post('id');
      $data =[
        'status' => 2,
      ];
      $this->Perjalanan_model->ubah($id,$data);
    }
    public function batal()
    {
      $id = $this->input->post('id');
      $data =[
        'status' => 0,
      ];
      $this->Perjalanan_model->ubah($id,$data);
    }
    public function selesai()
    {
      $id = $this->input->post('id');
      $data =[
        'status' => 1,
      ];
      $this->Perjalanan_model->ubah($id,$data);
    }
    public function getIdperjalanan()
    {
      $id = $this->input->post('id');
      $data = $this->Perjalanan_model->getId($id);
      echo json_encode($data);
    }
    public function simpanPengeluaran()
    {
      $nama_pengeluaran = $this->input->post('nama_pengeluaran');
      $jml_pengeluaran = $this->input->post('jml_pengeluaran');
      $perjalanan = $this->input->post('id_perjalanan');
      if ($nama_pengeluaran == '') {
  		  $result['pesan'] ="Nama Pengeluaran Harus Diisi";
  		}elseif ($jml_pengeluaran == '') {
        $result['pesan'] ="Jumlah Pengeluaran Harus Diisi";
      }else {
  			$result['pesan'] ="";
        $data = [
          'nama_pengeluaran' => $nama_pengeluaran,
          'jml_pengeluaran' => $jml_pengeluaran,
          'perjalanan' => $perjalanan,
        ];
  			$this->Pengeluaran_model->tambah($data);
  		}
      echo json_encode($result);
    }
    public function viewPengeluaran()
    {
      $id = $this->input->post('id');
      $data = $this->Pengeluaran_model->view($id);
      echo json_encode($data);
    }
    public function hapusPengeluaran()
    {
      $id = $this->input->post('id');
      $this->Pengeluaran_model->hapus($id);
    }
}
