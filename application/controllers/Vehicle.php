<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vehicle extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Vehicle_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('auth');
		}
	}

	public function index()
	{
		$data['title'] = "Vehicle";
		$data['user'] = $this->Auth_model->success_login();
    $this->load->view('templates/header',$data);
    $this->load->view('vehicle/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Vehicle_model->view();
		echo json_encode($data);
	}
	public function tambah()
	{
		$no_plat = $this->input->post('no_plat');
		$merk = $this->input->post('merk');
		$tahun_perakitan = $this->input->post('tahun_perakitan');
		$isi_silinder = $this->input->post('isi_silinder');
		$no_rangka = $this->input->post('no_rangka');
		$no_mesin = $this->input->post('no_mesin');
    $no_bpkb = $this->input->post('no_bpkb');
    $warna = $this->input->post('warna');
		if ($no_plat == '') {
		  $result['pesan'] ="No Plat Harus Diisi";
		}elseif ($merk=='') {
		  $result['pesan'] ="Merk Harus Diisi";
		}elseif ($tahun_perakitan =='') {
			$result['pesan'] ="Tahun Perakitan Harus Diisi";
		}elseif ($isi_silinder =='') {
			$result['pesan'] = "Isi Silinder Harus Diisi";
		}elseif ($no_rangka =='') {
			$result['pesan'] ="No Rangka Harus Diisi";
    }elseif ($no_mesin =='') {
      $result['pesan'] ="No Mesin Harus Diisi";
    }elseif ($no_bpkb =='') {
      $result['pesan'] ="No BPKB Harus Diisi";
    }elseif ($warna =='') {
      $result['pesan'] ="Warna Harus Diisi";
		}else {
			$result['pesan'] ="";
      $data = [
          'no_plat' => strtoupper($no_plat),
          'merk' => strtoupper($merk),
          'tahun_perakitan' => strtoupper($tahun_perakitan),
          'isi_silinder' => strtoupper($isi_silinder),
          'no_rangka' => strtoupper($no_rangka),
          'no_mesin' => strtoupper($no_mesin),
          'no_bpkb' => strtoupper($no_bpkb),
          'warna' => strtoupper($warna),
       ];
			$this->Vehicle_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Vehicle_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('e_vehicle');
    $no_plat = $this->input->post('e_no_plat');
    $merk = $this->input->post('e_merk');
    $tahun_perakitan = $this->input->post('e_tahun_perakitan');
    $isi_silinder = $this->input->post('e_isi_silinder');
    $no_rangka = $this->input->post('e_no_rangka');
    $no_mesin = $this->input->post('e_no_mesin');
    $no_bpkb = $this->input->post('e_no_bpkb');
    $warna = $this->input->post('e_warna');
    if ($no_plat == '') {
      $result['pesan'] ="No Plat Harus Diisi";
    }elseif ($merk=='') {
      $result['pesan'] ="Merk Harus Diisi";
    }elseif ($tahun_perakitan =='') {
      $result['pesan'] ="Tahun Perakitan Harus Diisi";
    }elseif ($isi_silinder =='') {
      $result['pesan'] = "Isi Silinder Harus Diisi";
    }elseif ($no_rangka =='') {
      $result['pesan'] ="No Rangka Harus Diisi";
    }elseif ($no_mesin =='') {
      $result['pesan'] ="No Mesin Harus Diisi";
    }elseif ($no_bpkb =='') {
      $result['pesan'] ="No BPKB Harus Diisi";
    }elseif ($warna =='') {
      $result['pesan'] ="Warna Harus Diisi";
		}else {
			$result['pesan'] ="";
      $data = [
          'no_plat' => strtoupper($no_plat),
          'merk' => strtoupper($merk),
          'tahun_perakitan' => strtoupper($tahun_perakitan),
          'isi_silinder' => strtoupper($isi_silinder),
          'no_rangka' => strtoupper($no_rangka),
          'no_mesin' => strtoupper($no_mesin),
          'no_bpkb' => strtoupper($no_bpkb),
          'warna' => strtoupper($warna),
       ];
			$this->Vehicle_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Vehicle_model->hapus($id);
	}
	public function download()
	{
		$vehicle = $this->Vehicle_model->view();
		$this->load->library('pdf');
		$this->pdf->set_option('isRemoteEnabled', TRUE);
		$this->pdf->setPaper('F4', 'landscape');
		$this->pdf->filename = "laporan-data-vehicle.pdf";
		$this->pdf->load_view('vehicle/cetak', compact('vehicle'));
	}

}
