<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pengeluaran_model extends CI_Model
{
  public function viewAll()
  {
    $this->db->select('*');
    $this->db->from('pengeluaran');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function view($id)
  {
    $this->db->select('*');
    $this->db->from('pengeluaran');
    $this->db->where('perjalanan',$id);
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('pengeluaran',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('pengeluaran', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('pengeluaran', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('pengeluaran', $data, array('id' => $id));
  }


}
