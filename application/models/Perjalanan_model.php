<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Perjalanan_model extends CI_Model
{
  var $table = 'perjalanan';
  var $column_order = array(null,'no_plat','merk', 'no_pengajuan','nama','nama_driver','tgl_berangkat','keperluan','tujuan','status_perjalanan'); //set column field database for datatable orderable
  var $column_search = array('no_plat','merk', 'no_pengajuan','nama','nama_driver','tgl_berangkat','keperluan','tujuan','status_perjalanan'); //set column field database for datatable searchable
  var $order = array('id_perjalanan' => 'asc'); // default order

  public function __construct()
  {
      parent::__construct();
      $this->load->database();
  }

  private function _get_datatables_query()
  {
      $this->db->select('driver.nama as nama_driver, perjalanan.id as id_perjalanan, pengajuan.no_pengajuan,pengajuan.tgl_pengajuan, karyawan.nama,pengajuan.keperluan, pengajuan.tgl_berangkat, pengajuan.id as id_pengajuan, pengajuan.tujuan, perjalanan.status as status_perjalanan, vehicle.no_plat,vehicle.merk');
      $this->db->from($this->table);
      $this->db->join('driver', 'perjalanan.driver = driver.id');
      $this->db->join('vehicle', 'perjalanan.vehicle = vehicle.id');
      $this->db->join('pengajuan', 'perjalanan.pengajuan = pengajuan.id');
      $this->db->join('karyawan', 'pengajuan.karyawan = karyawan.id');
      $this->db->order_by('pengajuan.tgl_berangkat','DESC');
      $i = 0;

      foreach ($this->column_search as $item) // loop column
      {
          if($_POST['search']['value']) // if datatable send POST for search
          {

              if($i===0) // first loop
              {
                  $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                  $this->db->like($item, $_POST['search']['value']);
              }
              else
              {
                  $this->db->or_like($item, $_POST['search']['value']);
              }

              if(count($this->column_search) - 1 == $i) //last loop
                  $this->db->group_end(); //close bracket
          }
          $i++;
      }

      if(isset($_POST['order'])) // here order processing
      {
          $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
          $order = $this->order;
          $this->db->order_by(key($order), $order[key($order)]);
      }
  }

  function get_datatables()
  {
      $this->_get_datatables_query();
      if(isset($_POST["length"]) && $_POST["length"] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
  }

  function count_filtered()
  {
      $this->_get_datatables_query();
      $query = $this->db->get();
      return $query->num_rows();
  }

  public function count_all()
  {
      $this->db->from($this->table);
      return $this->db->count_all_results();
  }

  public function tambah($data)
  {
    return $this->db->insert('perjalanan',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('perjalanan', array("id" => $id));
  }
  public function getById($id)
  {
    $this->db->select('pengajuan.no_pengajuan,pengajuan.tgl_pengajuan, karyawan.nama,pengajuan.keperluan, pengajuan.tgl_berangkat, pengajuan.id as id_pengajuan, pengajuan.status as status_p');
    $this->db->from('pengajuan');
    $this->db->join('karyawan', 'karyawan.id = pengajuan.karyawan');
    $this->db->where('pengajuan.id',$id);
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function getToday($today)
  {
    $this->db->select('driver.nama as nama_driver, perjalanan.id as id_perjalanan, pengajuan.no_pengajuan,pengajuan.tgl_pengajuan, karyawan.nama,pengajuan.keperluan, pengajuan.tgl_berangkat, pengajuan.id as id_pengajuan, pengajuan.tujuan, perjalanan.status as status_perjalanan, vehicle.no_plat,vehicle.merk');
    $this->db->from($this->table);
    $this->db->join('driver', 'perjalanan.driver = driver.id');
    $this->db->join('vehicle', 'perjalanan.vehicle = vehicle.id');
    $this->db->join('pengajuan', 'perjalanan.pengajuan = pengajuan.id');
    $this->db->join('karyawan', 'pengajuan.karyawan = karyawan.id');
    $this->db->order_by('pengajuan.tgl_berangkat','DESC');
    $this->db->where('pengajuan.tgl_berangkat',$today);
    $this->db->limit(5);
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function getTodayDriver($today,$id)
  {
    $this->db->select('driver.nama as nama_driver, perjalanan.id as id_perjalanan, pengajuan.no_pengajuan,pengajuan.tgl_pengajuan, karyawan.nama,pengajuan.keperluan, pengajuan.tgl_berangkat, pengajuan.id as id_pengajuan, pengajuan.tujuan, perjalanan.status as status_perjalanan, vehicle.no_plat,vehicle.merk');
    $this->db->from($this->table);
    $this->db->join('driver', 'perjalanan.driver = driver.id');
    $this->db->join('vehicle', 'perjalanan.vehicle = vehicle.id');
    $this->db->join('pengajuan', 'perjalanan.pengajuan = pengajuan.id');
    $this->db->join('karyawan', 'pengajuan.karyawan = karyawan.id');
    $this->db->order_by('pengajuan.tgl_berangkat','DESC');
    $this->db->where('pengajuan.tgl_berangkat',$today);
    $this->db->where('perjalanan.driver',$id);
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function getAllById($id)
  {
    $this->db->select('driver.nama as nama_driver, perjalanan.id as id_perjalanan, pengajuan.no_pengajuan,pengajuan.tgl_pengajuan, karyawan.nama,pengajuan.keperluan, pengajuan.tgl_berangkat, pengajuan.id as id_pengajuan, pengajuan.tujuan, perjalanan.status as status_perjalanan, vehicle.no_plat,vehicle.merk');
    $this->db->from($this->table);
    $this->db->join('driver', 'perjalanan.driver = driver.id');
    $this->db->join('vehicle', 'perjalanan.vehicle = vehicle.id');
    $this->db->join('pengajuan', 'perjalanan.pengajuan = pengajuan.id');
    $this->db->join('karyawan', 'pengajuan.karyawan = karyawan.id');
    $this->db->order_by('pengajuan.tgl_berangkat','DESC');
    $this->db->where('pengajuan.karyawan',$id);
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function getAllByIdDriver($id)
  {
    $this->db->select('driver.nama as nama_driver, perjalanan.id as id_perjalanan, pengajuan.no_pengajuan,pengajuan.tgl_pengajuan, karyawan.nama,pengajuan.keperluan, pengajuan.tgl_berangkat, pengajuan.id as id_pengajuan, pengajuan.tujuan, perjalanan.status as status_perjalanan, vehicle.no_plat,vehicle.merk');
    $this->db->from($this->table);
    $this->db->join('driver', 'perjalanan.driver = driver.id');
    $this->db->join('vehicle', 'perjalanan.vehicle = vehicle.id');
    $this->db->join('pengajuan', 'perjalanan.pengajuan = pengajuan.id');
    $this->db->join('karyawan', 'pengajuan.karyawan = karyawan.id');
    $this->db->order_by('pengajuan.tgl_berangkat','DESC');
    $this->db->where('perjalanan.driver',$id);
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('perjalanan', $data, array('id' => $id));
  }
  public function checkExist($pengajuan)
  {
      return $this->db->get_where('perjalanan',array('pengajuan'=>$pengajuan))->num_rows();
  }
  public function getId($id)
  {
    $query = $this->db->escape($this->db->get_where('perjalanan', array('id' => $id)));
    return $query->row_array();
  }
  public function cetakByTanggal($tgl_awal,$tgl_akhir)
  {
    $this->db->select('SUM(jml_pengeluaran) AS total, pengeluaran.jml_pengeluaran ,driver.nama as nama_driver, perjalanan.id as id_perjalanan, pengajuan.no_pengajuan,pengajuan.tgl_pengajuan, karyawan.nama,pengajuan.keperluan, pengajuan.tgl_berangkat, pengajuan.id as id_pengajuan, pengajuan.tujuan, perjalanan.status as status_perjalanan, vehicle.no_plat,vehicle.merk');
    $this->db->from($this->table);
    $this->db->join('driver', 'perjalanan.driver = driver.id');
    $this->db->join('vehicle', 'perjalanan.vehicle = vehicle.id');
    $this->db->join('pengajuan', 'perjalanan.pengajuan = pengajuan.id');
    $this->db->join('karyawan', 'pengajuan.karyawan = karyawan.id');
    $this->db->join('pengeluaran', 'pengeluaran.perjalanan = perjalanan.id');
    $this->db->where("tgl_berangkat between '$tgl_awal' AND '$tgl_akhir'");
    $this->db->group_by('no_pengajuan');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function getDetail($id)
  {
    $this->db->select('driver.nama as nama_driver, perjalanan.id as id_perjalanan, pengajuan.no_pengajuan,pengajuan.tgl_pengajuan, karyawan.nama,pengajuan.keperluan, pengajuan.tgl_berangkat, pengajuan.id as id_pengajuan, pengajuan.tujuan, perjalanan.status as status_perjalanan, vehicle.no_plat,vehicle.merk');
    $this->db->from($this->table);
    $this->db->join('driver', 'perjalanan.driver = driver.id');
    $this->db->join('vehicle', 'perjalanan.vehicle = vehicle.id');
    $this->db->join('pengajuan', 'perjalanan.pengajuan = pengajuan.id');
    $this->db->join('karyawan', 'pengajuan.karyawan = karyawan.id');
    $this->db->where('perjalanan.id',$id);
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
}
