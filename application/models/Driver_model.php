<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Driver_model extends CI_Model
{
  var $table = 'driver';
      var $column_order = array(null,'nama','email','no_sim','jk','tempat_lahir','tgl_lahir','status'); //set column field database for datatable orderable
      var $column_search = array('nama','email','no_sim','jk','tempat_lahir','tgl_lahir','status'); //set column field database for datatable searchable
      var $order = array('id' => 'asc'); // default order

      public function __construct()
      {
          parent::__construct();
          $this->load->database();
      }

      private function _get_datatables_query()
      {

          $this->db->from($this->table);
          $i = 0;

          foreach ($this->column_search as $item) // loop column
          {
              if($_POST['search']['value']) // if datatable send POST for search
              {

                  if($i===0) // first loop
                  {
                      $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                      $this->db->like($item, $_POST['search']['value']);
                  }
                  else
                  {
                      $this->db->or_like($item, $_POST['search']['value']);
                  }

                  if(count($this->column_search) - 1 == $i) //last loop
                      $this->db->group_end(); //close bracket
              }
              $i++;
          }

          if(isset($_POST['order'])) // here order processing
          {
              $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
          }
          else if(isset($this->order))
          {
              $order = $this->order;
              $this->db->order_by(key($order), $order[key($order)]);
          }
      }

      function get_datatables()
      {
          $this->_get_datatables_query();
          if(isset($_POST["length"]) && $_POST["length"] != -1)
          $this->db->limit($_POST['length'], $_POST['start']);
          $query = $this->db->get();
          return $query->result();
      }

      function count_filtered()
      {
          $this->_get_datatables_query();
          $query = $this->db->get();
          return $query->num_rows();
      }

      public function count_all()
      {
          $this->db->from($this->table);
          return $this->db->count_all_results();
      }
      public function tambah($data)
      {
        return $this->db->insert('driver',$data);
      }
      public function hapus($id)
      {
        $this->_deleteImage($id);
         $this->db->delete('driver', array("id" => $id));
      }
      public function getById($id)
      {
        $query = $this->db->escape($this->db->get_where('driver', array('id' => $id)));
        return $query->row_array();
      }
      public function ubah($data,$id)
      {
        $this->db->update('driver', $data, array('id' => $id));
      }
      private function _deleteImage($id)
      {
          $admin = $this->db->get_where('driver',["id" => $id])->row();
          if ($admin->foto != "default.jpg") {
              $filename = explode(".", $admin->foto)[0];
              return array_map('unlink', glob(FCPATH . "assets/img/driver/$filename.*"));
            }
      }

    public function cetak()
    {
      $this->db->select('*');
      $this->db->from('driver');
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function getDriver($nama,$column)
    {
      $this->db->select('*');
      $this->db->limit(10);
      $this->db->from('driver');
      $this->db->like('nama', $nama);
      return $this->db->get()->result_array();
    }

    public function ubah_pass($id, $hash_con)
    {
      $this->db->set('password', $hash_con);
      $this->db->where('id', $id);
      $this->db->update('driver');
    }

}
