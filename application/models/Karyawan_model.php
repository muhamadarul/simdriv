<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{
  var $table = 'karyawan';
      var $column_order = array(null, 'nip','nama','nama_jabatan','email','status'); //set column field database for datatable orderable
      var $column_search = array('nip','nama','nama_jabatan','email','status'); //set column field database for datatable searchable
      var $order = array('id_karyawan' => 'asc'); // default order

      public function __construct()
      {
          parent::__construct();
          $this->load->database();
      }

      private function _get_datatables_query()
      {
          $this->db->select('*, karyawan.id as id_karyawan');
          $this->db->from($this->table);
          $this->db->join('jabatan', 'karyawan.jabatan= jabatan.id');


          $i = 0;

          foreach ($this->column_search as $item) // loop column
          {
              if($_POST['search']['value']) // if datatable send POST for search
              {

                  if($i===0) // first loop
                  {
                      $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                      $this->db->like($item, $_POST['search']['value']);
                  }
                  else
                  {
                      $this->db->or_like($item, $_POST['search']['value']);
                  }

                  if(count($this->column_search) - 1 == $i) //last loop
                      $this->db->group_end(); //close bracket
              }
              $i++;
          }

          if(isset($_POST['order'])) // here order processing
          {
              $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
          }
          else if(isset($this->order))
          {
              $order = $this->order;
              $this->db->order_by(key($order), $order[key($order)]);
          }
      }

      function get_datatables()
      {
          $this->_get_datatables_query();
          if(isset($_POST["length"]) && $_POST["length"] != -1)
          $this->db->limit($_POST['length'], $_POST['start']);
          $query = $this->db->get();
          return $query->result();
      }

      function count_filtered()
      {
          $this->_get_datatables_query();
          $query = $this->db->get();
          return $query->num_rows();
      }

      public function count_all()
      {
          $this->db->from($this->table);
          return $this->db->count_all_results();
      }
      public function tambah($data)
      {
        return $this->db->insert('karyawan',$data);
      }
      public function hapus($id)
      {
        $this->_deleteImage($id);
         $this->db->delete('karyawan', array("id" => $id));
      }
      public function getById($id)
      {
        $query = $this->db->escape($this->db->get_where('karyawan', array('id' => $id)));
        return $query->row_array();
      }
      public function getDetail($id)
      {
        $this->db->select('*, karyawan.id as id_karyawan');
        $this->db->from($this->table);
        $this->db->join('jabatan', 'karyawan.jabatan= jabatan.id');
        $this->db->where('karyawan.id',$id);
        $query = $this->db->escape($this->db->get());
        return $query->row_array();
      }
      public function ubah($data,$id)
      {
        $this->db->update('karyawan', $data, array('id' => $id));
      }
      private function _deleteImage($id)
      {
          $admin = $this->db->get_where('karyawan',["id" => $id])->row();
          if ($admin->foto != "default.jpg") {
              $filename = explode(".", $admin->foto)[0];
              return array_map('unlink', glob(FCPATH . "assets/img/karyawan/$filename.*"));
            }
      }

      function getRows($params = array()){
        $this->db->select('*');
        $this->db->from('karyawan');

        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach($params['conditions'] as $key => $value){
                $this->db->where($key,$value);
            }
        }

        if(array_key_exists("id_alumni",$params)){
            $this->db->where('id_alumni',$params['id_alumni']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $this->db->count_all_results();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->row_array():false;
            }else{
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():false;
            }
        }

        //return fetched data
        return $result;
    }
    public function cetak()
    {
      $this->db->select('*, karyawan.id as id_karyawan');
      $this->db->from('karyawan');
      $this->db->join('jabatan', 'karyawan.jabatan= jabatan.id');
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function ubah_pass($id, $hash_con)
    {
      $this->db->set('password', $hash_con);
      $this->db->where('id', $id);
      $this->db->update('karyawan');
    }
    public function getKaryawan($nama,$column)
    {
      $this->db->select('*');
      $this->db->limit(10);
      $this->db->from('karyawan');
      $this->db->where('jabatan',1);
      $this->db->like('nama', $nama);
      return $this->db->get()->result_array();
    }


}
