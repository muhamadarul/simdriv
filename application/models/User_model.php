<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
  public function view()
  {
    $this->db->select('username,status,nama,id,hak_akses.level as nama_level');
    $this->db->from('user');
    $this->db->join('hak_akses', 'hak_akses.id_akses = user.level');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('user',$data);
  }
  public function cekUsername($username)
  {
    $this->db->select('username');
    $this->db->from('user');
    $this->db->where('username',$username);
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function hapus($id)
  {
     return $this->db->delete('user', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('user', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('user', $data, array('id' => $id));
  }
  public function getAkses()
	{
		$this->db->select('*');
		$this->db->from('hak_akses');
		$query = $this->db->escape($this->db->get());
		return $query->result_array();
	}
  public function getDetail($id)
  {
    $this->db->select('*, hak_akses.level as nama_akses');
    $this->db->from('user');
    $this->db->join('hak_akses', 'hak_akses.id_akses = user.level');
    $this->db->where('user.id',$id);
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
}
