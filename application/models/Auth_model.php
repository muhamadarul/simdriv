<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{

    public function login($username, $password)
    {
        $user = $this->db->get_where('user', ['username' => $username])->row_array();
        return $user;
    }

    public function success_login()
    {
        $user = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        return $user;
    }
    public function login_karyawan($email, $password)
    {
        $user = $this->db->get_where('karyawan', ['email' => $email])->row_array();
        return $user;
    }

    public function success_login_karyawan()
    {
        $user = $this->db->get_where('karyawan', ['email' => $this->session->userdata('email')])->row_array();
        return $user;
    }
    public function login_driver($email, $password)
    {
        $user = $this->db->get_where('driver', ['email' => $email])->row_array();
        return $user;
    }

    public function success_login_driver()
    {
        $user = $this->db->get_where('driver', ['email' => $this->session->userdata('email_driver')])->row_array();
        return $user;
    }


}
