<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Vehicle_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('vehicle');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('vehicle',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('vehicle', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('vehicle', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('vehicle', $data, array('id' => $id));
  }
  public function getKendaraan($no_plat,$column)
  {
    $this->db->select('*');
    $this->db->limit(10);
    $this->db->from('vehicle');
    $this->db->like('no_plat', $no_plat);
    return $this->db->get()->result_array();
  }
}
