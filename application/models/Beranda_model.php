<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Beranda_model extends CI_Model
{
  public function countKaryawan()
  {
      $query = $this->db->get('karyawan');
      if ($query->num_rows() > 0) {
          return $query->num_rows();
      } else {
          return 0;
      }
  }
  public function countDriver()
  {
      $query = $this->db->get('driver');
      if ($query->num_rows() > 0) {
          return $query->num_rows();
      } else {
          return 0;
      }
  }
  public function countVehicle()
  {
      $query = $this->db->get('vehicle');
      if ($query->num_rows() > 0) {
          return $query->num_rows();
      } else {
          return 0;
      }
  }
  public function countApproved($id)
  {
    $query =  $this->db->escape($this->db->get_where('pengajuan', array('status' => 1, 'karyawan' => $id)));
    if ($query->num_rows() > 0) {
        return $query->num_rows();
    } else {
        return 0;
    }
  }
  public function countRejected($id)
  {
    $query =  $this->db->escape($this->db->get_where('pengajuan', array('status' => 0,'karyawan' => $id)));
    if ($query->num_rows() > 0) {
        return $query->num_rows();
    } else {
        return 0;
    }
  }

}
