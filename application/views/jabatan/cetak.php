<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Jabatan</title>
</head>
<body>
    <h3><center>DAFTAR JABATAN</center></h3>
    <table border="1" cellspacing="0" cellpadding="5" width="100%">
        <thead>
            <tr>
              <th>No.</th>
              <th>Nama Jabatan</th>
            </tr>
        </thead>
        <tbody>
            <?php if(is_array($jabatan) || is_object($jabatan) ):?>
            <?php
            $no=0;
            foreach ($jabatan as $data) {
                $no++;
                echo "<tr>";
                    echo "<td><center>".$no."</center></td>";
                    echo "<td>".$data['nama_jabatan']."</td>";
                echo "</tr>";
            }
            ?>
          <?php endif; ?>

        </tbody>
    </table>
</body>
</html>
