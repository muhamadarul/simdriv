<!-- MAIN -->
<div class="main">
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <!-- OVERVIEW -->
      <div class="panel panel-headline">
        <div class="panel-heading">
          <h3 class="panel-title">Data Jabatan</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <div class="text-left">
                <a href="<?= base_url('jabatan/download');?>" target="_blank" class="btn btn-danger"><i class="fa fa-download"></i>&nbsp;   Download Data</a>
              </div>
            </div>
            <div class="col-md-6">
              <div class="text-right">
                <a href="#" data-toggle="modal" data-target="#modalTambah" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;   Tambah Data</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <br>
              <div class="panel">
                <div class="panel-body">
                  <table id="table" class="table table-striped">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Nama Jabatan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
              <!-- END RECENT PURCHASES -->
            </div>
          </div>
        </div>
      </div>
      <!-- END OVERVIEW -->
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>

<!-- Modal Tambah-->
<form id="tambah" method="post">
  <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Tambah Data Jabatan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <center><font color="red"><p id="pesan"></p></font></center>
          <div class="form-group">
            <label class="col-form-label">Nama Jabatan :</label>
            <input type="text" class="form-control" id="nama_jabatan" name="nama_jabatan">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="button" id="#btnSave" onclick="save()"  class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Ubah-->
<form id="ubah" method="post">
  <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Ubah Data Jabatan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center><font color="red"><p id="e_pesan"></p></font></center>
            <div class="form-group" hidden>
              <label class="col-form-label">Id :</label>
              <input type="text" class="form-control" id="id" name="id">
            </div>
            <div class="form-group">
              <label class="col-form-label">Nama Jabatan :</label>
              <input type="text" class="form-control" id="nama_jabatan2" name="nama_jabatan2">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" id="#btnUpdate" onclick="update()"  class="btn btn-success"><i class="fa fa-save"></i>&nbsp; Ubah</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script src="<?= base_url(); ?>assets/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
var save_method; //for save method string
$(document).ready(function() {
  var table = $('#table').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "responsive": true,
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "order": [], //Initial no order.
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": "<?php echo base_url('jabatan/ajax_list')?>",
          "type": "POST",
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      {
          "targets": [ 0 ], //first column / numbering column
          "orderable": false, //set not orderable
      },
      ],
  });
});

function eraseText() {
     document.getElementById("nama_jabatan").value = "";
}
function eraseTextEdit() {
      document.getElementById("nama_jabatan2").value = "";
}
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}
function save()
{
    $('#btnSave').text('Menyimpan...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable
    var url;
    url = "<?php echo site_url('jabatan/tambah')?>";
    // var file_data = $('#foto').prop('files')[0];
    var form_data = new FormData(document.getElementById("tambah"));
    // form_data.append('file', file_data);

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: form_data,
        dataType: "JSON",
        contentType: false,
        processData:false,
        cache:false,
        success: function(data)
        {

            $('#pesan').html(data.pesan);
            if(data.pesan=="") //if success close modal and reload ajax table
            {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil ditambahkan!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
                reload_table();
                $('#btnSave').text('simpan'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable
            }
            $('#btnSave').text('simpan'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
        }
    });
}
function edit(id)
{

    $('#ubah')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
      $('#modalEdit').modal('show'); // show bootstrap modal when complete loaded
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url('jabatan/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nama_jabatan2"]').val(data.nama_jabatan);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
function hapus(id) {
  Swal.fire({
      title: "Apakah anda yakin?",
      text: "data akan dihapus!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          data:'id='+id,
          url:'<?= base_url().'jabatan/hapus'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil dihapus!',
                type: 'success'
            });
              reload_table();
          }
        });
      }
  });
}
function update() {
  $('#btnUpdate').text('Mengubah...'); //change button text
  $('#btnUpdate').attr('disabled',true); //set button disable
  // var file_data = $('#foto').prop('files')[0];
  var form_data2 = new FormData(document.getElementById("ubah"));
  // form_data.append('file', file_data);

  // ajax adding data to database
  $.ajax({
      url : "<?php echo base_url('jabatan/ubah')?>",
      type: "POST",
      data: form_data2,
      dataType: "JSON",
      contentType: false,
      processData:false,
      cache:false,
      success: function(data)
      {
        console.log(data);
          $('#e_pesan').html(data.pesan);
          if(data.pesan=="") //if success close modal and reload ajax table
          {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil diubah!',
                type: 'success'
            });
            setTimeout(function () {
              $("[data-dismiss=modal]").trigger({
                type: "click"
              });
            },100)
              reload_table();
              $('#btnUpdate').text('ubah'); //change button text
              $('#btnUpdate').attr('disabled',false); //set button enable
          }
          $('#btnUpdate').text('ubah'); //change button text
          $('#btnUpdate').attr('disabled',false); //set button enable
      }
  });
}
</script>
