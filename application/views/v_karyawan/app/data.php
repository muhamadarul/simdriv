<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">

  <!--begin::Content-->
  <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Toolbar-->
    <div class="toolbar" id="kt_toolbar">
      <!--begin::Container-->
      <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <!--begin::Page title-->
        <div class="d-flex align-items-center me-3">
          <!--begin::Title-->
          <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Application Approvement
          <!--begin::Separator-->
          <span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
          <!--end::Separator-->
          <!--begin::Description-->
          <small class="text-muted fs-7 fw-bold my-1 ms-1">Welcome ! <?= $karyawan['nama'];?></small>
          <!--end::Description--></h1>
          <!--end::Title-->
        </div>
        <!--end::Page title-->
      </div>
      <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
      <!--begin::Container-->
      <div id="kt_content_container" class="container">

        <!--begin::Row-->
        <div class="row g-5 gx-xxl-8">
          <!--begin::Col-->
          <div class="col-xxl-12">
            <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
							<!--begin::Card header-->
							<div class="card-header cursor-pointer">
								<!--begin::Card title-->
								<div class="card-title m-0">
									<h3 class="fw-bolder m-0"> Application Data</h3>
								</div>
								<!--end::Card title-->
								<!--begin::Action-->
								<!-- <a href="" data-bs-toggle="modal" data-bs-target="#edit_profile" class="btn btn-primary align-self-center">Edit Profile</a> -->
								<!--end::Action-->
							</div>
							<!--begin::Card header-->

							<!--begin::Card body-->
							<div class="card-body p-9">
                <!--begin::Row-->
                <div class="row mb-7">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <td>Number</td>
                        <td>Application Number</td>
                        <td>Date Approvement</td>
                        <td>Destination</td>
                        <td>Reason</td>
                        <td>Date of Departure</td>
                        <td>Status</td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no = 0; foreach($app as $row) : $no++ ?>
                      <tr>
                        <td><?= $no; ?></td>
                        <td><?= $row['no_pengajuan']; ?></td>
                        <td><?= $row['tgl_pengajuan']; ?></td>
                        <td><?= $row['tujuan']; ?></td>
                        <td><?= $row['keperluan']; ?></td>
                        <td><?= $row['tgl_berangkat']; ?></td>
                        <?php if ($row['status_p']==1): ?>
                          <td>Approved</td>
                        <?php elseif ($row['status_p']==2) :?>
                          <td>Pending</td>
                        <?php else: ?>
                          <td>Rejected</td>
                        <?php endif; ?>
                      </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
							</div>
							<!--end::Card body-->
						</div>
						<!--end::details View-->
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
      </div>
      <!--end::Container-->
    </div>
    <!--end::Post-->
  </div>
  <!--end::Content-->
<script type="text/javascript">
  $("#example1").DataTable();
</script>
