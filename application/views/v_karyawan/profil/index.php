<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">

  <!--begin::Content-->
  <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Toolbar-->
    <div class="toolbar" id="kt_toolbar">
      <!--begin::Container-->
      <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <!--begin::Page title-->
        <div class="d-flex align-items-center me-3">
          <!--begin::Title-->
          <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Your Profile
          <!--begin::Separator-->
          <span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
          <!--end::Separator-->
          <!--begin::Description-->
          <small class="text-muted fs-7 fw-bold my-1 ms-1">Welcome ! <?= $karyawan['nama'];?></small>
          <!--end::Description--></h1>
          <!--end::Title-->
        </div>
        <!--end::Page title-->
      </div>
      <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
      <!--begin::Container-->
      <div id="kt_content_container" class="container">

        <!--begin::Row-->
        <div class="row g-5 gx-xxl-8">
          <!--begin::Col-->
          <div class="col-xxl-12">
            <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
							<!--begin::Card header-->
							<div class="card-header cursor-pointer">
								<!--begin::Card title-->
								<div class="card-title m-0">
									<h3 class="fw-bolder m-0">Profile Details</h3>
								</div>
								<!--end::Card title-->
								<!--begin::Action-->
								<!-- <a href="" data-bs-toggle="modal" data-bs-target="#edit_profile" class="btn btn-primary align-self-center">Edit Profile</a> -->
								<!--end::Action-->
							</div>
							<!--begin::Card header-->
							<!--begin::Card body-->
							<div class="card-body p-9">
                <div class="row">
                  <div class="col-6">
                    <!--begin::Row-->
                    <div class="row mb-7">
                      <!--begin::Label-->
                      <label class="col-lg-4 fw-bold text-muted">Full Name</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-dark"><?= $detail_karyawan['nama'];?></span>
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Row-->
                    <!--begin::Input group-->
                    <div class="row mb-7">
                      <!--begin::Label-->
                      <label class="col-lg-4 fw-bold text-muted">NIP</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <span class="fw-bolder fs-6 text-dark"><?= $detail_karyawan['nip'];?></span>
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-7">
                      <!--begin::Label-->
                      <label class="col-lg-4 fw-bold text-muted">Email
                      <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Phone number must be active"></i></label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 d-flex align-items-center">
                        <span class="fw-bolder fs-6 me-2"><?= $detail_karyawan['email'];?></span>
                        <?php if ($detail_karyawan['status']==1): ?>
                          <span class="badge badge-success">Active</span>
                        <?php else: ?>
                          <span class="badge badge-danger">Deactive</span>
                        <?php endif; ?>
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-7">
                      <!--begin::Label-->
                      <label class="col-lg-4 fw-bold text-muted">Date of Birth</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-dark"><?= $detail_karyawan['tempat_lahir'].','.' '.date('d F Y', strtotime($detail_karyawan['tgl_lahir']));?></span>
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-7">
                      <!--begin::Label-->
                      <label class="col-lg-4 fw-bold text-muted">SIM Number</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-dark"><?= $detail_karyawan['no_sim'];?></span>
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-7">
                      <!--begin::Label-->
                      <label class="col-lg-4 fw-bold text-muted">Gender</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8">
                        <?php if ($detail_karyawan['jk']=="Perempuan"): ?>
                          <span class="fw-bolder fs-6 text-dark">Female</span>
                        <?php else: ?>
                          <span class="fw-bolder fs-6 text-dark">Male</span>
                        <?php endif; ?>
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-10">
                      <!--begin::Label-->
                      <label class="col-lg-4 fw-bold text-muted">Address</label>
                      <!--begin::Label-->
                      <!--begin::Label-->
                      <div class="col-lg-8">
                        <span class="fw-bold fs-6"><?= $detail_karyawan['alamat'];?></span>
                      </div>
                      <!--begin::Label-->
                    </div>
                    <!--end::Input group-->
                    <div class="row mb-10">
                      <!--begin::Label-->
                      <label class="col-lg-4 fw-bold text-muted">Position</label>
                      <!--begin::Label-->
                      <!--begin::Label-->
                      <div class="col-lg-8">
                        <span class="fw-bold fs-6"><?= $detail_karyawan['nama_jabatan'];?></span>
                      </div>
                      <!--begin::Label-->
                    </div>
                    <!--end::Input group-->
                  </div>
                  <div class="col-6">
                      <img src="<?= base_url('');?>assets/img/karyawan/<?= $detail_karyawan['foto'];?>" alt="<?= $detail_karyawan['nama'];?>" />
                  </div>
                </div>
							</div>
							<!--end::Card body-->
						</div>
						<!--end::details View-->
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
      </div>
      <!--end::Container-->
    </div>
    <!--end::Post-->
  </div>
  <!--end::Content-->


  <div class="modal fade" id="edit_profile" tabindex="-1" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-dialog-centered mw-650px">
      <!--begin::Modal content-->
      <div class="modal-content">
        <!--begin::Modal header-->
        <div class="modal-header" id="header_edit_profile">
          <!--begin::Modal title-->
          <h2>Edit Your Profile</h2>
          <!--end::Modal title-->
          <!--begin::Close-->
          <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
            <!--begin::Svg Icon | path: icons/stockholm/Navigation/Close.svg-->
            <span class="svg-icon svg-icon-1">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                  <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                  <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                </g>
              </svg>
            </span>
            <!--end::Svg Icon-->
          </div>
          <!--end::Close-->
        </div>
        <!--end::Modal header-->
        <!--begin::Form-->
        <form id="kt_modal_create_api_key_form" class="form" action="#">
          <!--begin::Modal body-->
          <div class="modal-body py-10 px-lg-17">
            <!--begin::Scroll-->
            <div class="scroll-y me-n7 pe-7" id="kt_modal_create_api_key_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_create_api_key_header" data-kt-scroll-wrappers="#kt_modal_create_api_key_scroll" data-kt-scroll-offset="300px">
              <!--begin::Input group-->
              <div class="mb-5 fv-row">
                <!--begin::Label-->
                <label class="required fs-5 fw-bold mb-2">NIP</label>
                <!--end::Label-->
                <!--begin::Input-->
                <input type="text" class="form-control form-control-solid" placeholder="Your NIP" name="nip" />
                <!--end::Input-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="mb-5 fv-row">
                <!--begin::Label-->
                <label class="required fs-5 fw-bold mb-2">Full Name</label>
                <!--end::Label-->
                <!--begin::Input-->
                <input type="text" class="form-control form-control-solid" placeholder="Your Full Name" name="nama" />
                <!--end::Input-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="d-flex flex-column mb-10 fv-row">
                <!--begin::Label-->
                <label class="required fs-5 fw-bold mb-2">Gender</label>
                <!--end::Label-->
                <!--begin::Select-->
                <select name="category" data-control="select2" data-hide-search="true" data-placeholder="Select Gender..." class="form-select form-select-solid">
                  <option value="">Select Gender</option>
                  <option value="Perempuan">Perempuan</option>
                  <option value="Laki-laki">Laki-laki</option>
                </select>
                <!--end::Select-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="mb-5 fv-row">
                <!--begin::Label-->
                <label class="required fs-5 fw-bold mb-2">Email</label>
                <!--end::Label-->
                <!--begin::Input-->
                <input type="text" class="form-control form-control-solid" placeholder="Your Email" name="email" />
                <!--end::Input-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="mb-5 fv-row">
                <!--begin::Label-->
                <label class="required fs-5 fw-bold mb-2">SIM Number</label>
                <!--end::Label-->
                <!--begin::Input-->
                <input type="text" class="form-control form-control-solid" placeholder="Your SIM Number" name="sim" />
                <!--end::Input-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="d-flex flex-column mb-5 fv-row">
                <!--begin::Label-->
                <label class="required fs-5 fw-bold mb-2">Address</label>
                <!--end::Label-->
                <!--begin::Input-->
                <textarea class="form-control form-control-solid" rows="3" name="alamat" placeholder="Your Address"></textarea>
                <!--end::Input-->
              </div>
              <!--end::Input group-->
            </div>
            <!--end::Scroll-->
          </div>
          <!--end::Modal body-->
          <!--begin::Modal footer-->
          <div class="modal-footer flex-center">
            <!--begin::Button-->
            <button type="reset" id="kt_modal_create_api_key_cancel" class="btn btn-white me-3">Discard</button>
            <!--end::Button-->
            <!--begin::Button-->
            <button type="submit" id="kt_modal_create_api_key_submit" class="btn btn-primary">
              <span class="indicator-label">Submit</span>
              <span class="indicator-progress">Please wait...
              <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
            </button>
            <!--end::Button-->
          </div>
          <!--end::Modal footer-->
        </form>
        <!--end::Form-->
      </div>
      <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
  </div>
  <!--end::Modal - Create Api Key-->
