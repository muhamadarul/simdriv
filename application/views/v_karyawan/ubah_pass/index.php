<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">

  <!--begin::Content-->
  <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Toolbar-->
    <div class="toolbar" id="kt_toolbar">
      <!--begin::Container-->
      <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <!--begin::Page title-->
        <div class="d-flex align-items-center me-3">
          <!--begin::Title-->
          <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Change Password
          <!--begin::Separator-->
          <span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
          <!--end::Separator-->
          <!--begin::Description-->
          <small class="text-muted fs-7 fw-bold my-1 ms-1">Welcome ! <?= $karyawan['nama'];?></small>
          <!--end::Description--></h1>
          <!--end::Title-->
        </div>
        <!--end::Page title-->
      </div>
      <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
      <!--begin::Container-->
      <div id="kt_content_container" class="container">

        <!--begin::Row-->
        <div class="row g-5 gx-xxl-8">
          <!--begin::Col-->
          <div class="col-xxl-12">
            <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
							<!--begin::Card header-->
							<div class="card-header cursor-pointer">
								<!--begin::Card title-->
								<div class="card-title m-0">
									<h3 class="fw-bolder m-0">Change Your Password</h3>
								</div>
								<!--end::Card title-->
								<!--begin::Action-->
								<!-- <a href="" data-bs-toggle="modal" data-bs-target="#edit_profile" class="btn btn-primary align-self-center">Edit Profile</a> -->
								<!--end::Action-->
							</div>
							<!--begin::Card header-->
              <form id="ubah" method="post">
							<!--begin::Card body-->
							<div class="card-body p-9">
                <!--begin::Row-->
                <div class="row mb-7">
                  <!--begin::Label-->
                  <label class="col-lg-4 fw-bold text-muted">Old Password</label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8">
                    <input type="password" class="form-control form-control-solid" placeholder="Old Password" name="password_lama" />
                  </div>
                  <!--end::Col-->
                </div>
                <!--end::Row-->
                <div class="row mb-7">
                  <!--begin::Label-->
                  <label class="col-lg-4 fw-bold text-muted">New Password</label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8">
                    <input type="password" class="form-control form-control-solid" placeholder="New Password" name="password_baru" />
                  </div>
                  <!--end::Col-->
                </div>
                <!--end::Row-->
                <div class="row mb-7">
                  <!--begin::Label-->
                  <label class="col-lg-4 fw-bold text-muted">Confirm Your Password</label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8">
                    <input type="password" class="form-control form-control-solid" placeholder="Confirm Password" name="password_baru2" />
                  </div>
                  <!--end::Col-->
                </div>
                <!--end::Row-->
                <input type="text" name="id" hidden value="<?= $karyawan['id'];?>">
                <div class="row mb-7">
                  <label class="col-lg-4 fw-bold text-muted"></label>

                  <!--begin::Col-->
                  <div class="col-lg-8">
                    <button type="submit" class="btn btn-primary">
                      <span class="indicator-label">Change</span>
                      <span class="indicator-progress">Please wait...
                      <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                  </div>
                  <!--end::Col-->
                </div>
                <!--end::Row-->
                </form>
                <div class="notification">
                <div class="row mb-7"  id="notification">
                  <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-10 p-6">
                    <!--begin::Icon-->
                    <!--begin::Svg Icon | path: icons/stockholm/Code/Warning-1-circle.svg-->
                    <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                        <rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />
                        <rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />
                      </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <!--end::Icon-->
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-stack flex-grow-1">
                      <!--begin::Content-->
                      <div class="fw-bold">
                        <h4 class="text-gray-800 fw-bolder">Warning!</h4>
                        <div id="message" class="fs-6 text-gray-600"></div>
                      </div>
                      <!--end::Content-->
                    </div>
                    <!--end::Wrapper-->
                  </div>
                  <!--end::Notice-->
                </div>
                </div>
							</div>
							<!--end::Card body-->
						</div>
						<!--end::details View-->
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
      </div>
      <!--end::Container-->
    </div>
    <!--end::Post-->
  </div>
  <!--end::Content-->
  <script type="text/javascript">
  $(document).ready(function() {
    $('.notification').hide();
  });
  $('#ubah').on('submit', function(event){
    event.preventDefault();
      $.ajax({
        type:'POST',
        url:"<?= base_url().'apps/change_password/action'?>",
        data:$(this).serialize(),
        dataType:'json',
        success:function(data){
          $('.notification').show();
          $('#message').html(data.message);
          if (data.message=="") {
            Swal.fire({
                text: "Password has been updated",
                icon: "success",
                buttonsStyling: false,
                confirmButtonText: "Ok, got it!",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
            $('.notification').hide();
            document.getElementById('password_lama').value = "";
            document.getElementById('password_baru').value = "";
            document.getElementById('password_baru2').value = "";
          }
        }
      })
    });
  </script>
