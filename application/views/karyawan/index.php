<!-- MAIN -->
<div class="main">
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <!-- OVERVIEW -->
      <div class="panel panel-headline">
        <div class="panel-heading">
          <h3 class="panel-title">Data Karyawan</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <div class="text-left">
                <a href="<?= base_url('karyawan/download');?>" target="_blank" class="btn btn-danger"><i class="fa fa-download"></i>&nbsp;   Download Data</a>
              </div>
            </div>
            <div class="col-md-6">
              <div class="text-right">
                <a href="#" data-toggle="modal" data-target="#modalTambah" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;   Tambah Data</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <br>
              <div class="panel">
                <div class="panel-body">
                  <table id="table" class="table table-striped">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
              <!-- END RECENT PURCHASES -->
            </div>
          </div>
        </div>
      </div>
      <!-- END OVERVIEW -->
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>

<!-- Modal Tambah-->
<form id="tambah" method="post">
  <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Tambah Data Karyawan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <center><font color="red"><p id="pesan"></p></font></center>
          <div class="form-group">
            <label class="col-form-label">NIP :</label>
            <input type="text" class="form-control" id="nip" name="nip">
          </div>
          <div class="form-group">
            <label class="col-form-label">Nama :</label>
            <input type="text" class="form-control" id="nama" name="nama">
          </div>
          <div class="form-group">
            <label class="col-form-label">Tempat Lahir :</label>
            <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir">
          </div>
          <div class="form-group">
            <label class="col-form-label">Tanggal Lahir :</label>
            <input type="text" class="form-control datepicker" id="tgl_lahir" name="tgl_lahir">
          </div>
          <div class="form-group">
            <label class="col-form-label">Jenis Kelamin :</label>
            <select class="form-control" name="jk">
              <option value="Laki-laki" selected>Laki-laki</option>
              <option value="Perempuan">Perempuan</option>
            </select>
          </div>
          <div class="form-group">
            <label class="col-form-label">Email :</label>
            <input type="email" class="form-control" id="email" name="email">
          </div>
          <div class="form-group">
            <label class="col-form-label">No SIM :</label>
            <input type="text" class="form-control" id="no_sim" name="no_sim">
          </div>
          <div class="form-group">
            <label class="col-form-label">Alamat :</label>
            <textarea name="alamat" class="form-control" rows="8" cols="80"></textarea>
          </div>
          <div class="form-group">
            <label class="col-form-label">Jabatan :</label>
            <select class="form-control" name="jabatan" id="jabatan">
            </select>
          </div>
          <div class="form-group">
            <label class="col-form-label">Foto :</label>
            <input type="file" class="form-control" id="foto" name="foto">
          </div>
          <div class="form-group">
            <label class="col-form-label">Password :</label>
            <input type="password" class="form-control" id="password" name="password">
          </div>
          <div class="form-group">
            <label class="col-form-label">Status :</label>
            <select class="form-control" name="status">
              <option value="1" selected>Aktif</option>
              <option value="0">Non-Aktif</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="button" id="#btnSave" onclick="save()"  class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Ubah-->
<form id="ubah" method="post">
  <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Ubah Data Karyawan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center><font color="red"><p id="e_pesan"></p></font></center>
            <div class="form-group" hidden>
              <label class="col-form-label">Id :</label>
              <input type="text" class="form-control" id="id" name="id">
            </div>
            <div class="form-group">
              <label class="col-form-label">NIP :</label>
              <input type="text" class="form-control" id="nip2" name="nip2">
            </div>
            <div class="form-group">
              <label class="col-form-label">Nama :</label>
              <input type="text" class="form-control" id="nama2" name="nama2">
            </div>
            <div class="form-group">
              <label class="col-form-label">Tempat Lahir :</label>
              <input type="text" class="form-control" id="tempat_lahir2" name="tempat_lahir2">
            </div>
            <div class="form-group">
              <label class="col-form-label">Tanggal Lahir :</label>
              <input type="text" class="form-control datepicker" id="tgl_lahir2" name="tgl_lahir2">
            </div>
            <div class="form-group">
              <label class="col-form-label">Jenis Kelamin :</label>
              <select class="form-control" name="jk2">
                <option value="Laki-laki" selected>Laki-laki</option>
                <option value="Perempuan">Perempuan</option>
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label">Email :</label>
              <input type="email" class="form-control" id="email2" name="email2">
            </div>
            <div class="form-group">
              <label class="col-form-label">No SIM :</label>
              <input type="text" class="form-control" id="no_sim2" name="no_sim2">
            </div>
            <div class="form-group">
              <label class="col-form-label">Alamat :</label>
              <textarea name="alamat2" class="form-control" rows="8" cols="80"></textarea>
            </div>
            <div class="form-group">
              <label class="col-form-label">Jabatan :</label>
              <select class="form-control" name="jabatan2" id="jabatan2">
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label">Foto : | Isi Jika Ingin Mengubah</label>
              <input type="file" class="form-control" id="foto2" name="foto2">
            </div>
            <div class="form-group" hidden>
              <label class="col-form-label">Foto :</label>
              <input type="name" class="form-control" id="foto_lama" name="foto_lama">
            </div>
            <div class="form-group">
              <label class="col-form-label">Password : | Isi Jika Ingin Mengubah</label>
              <input type="password" class="form-control" id="password2" name="password2">
            </div>
            <div class="form-group">
              <label class="col-form-label">Status :</label>
              <select class="form-control" name="status2">
                <option value="1" selected>Aktif</option>
                <option value="0">Non-Aktif</option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" id="#btnUpdate" onclick="update()"  class="btn btn-success"><i class="fa fa-save"></i>&nbsp; Ubah</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Detail-->
<form id="detail" method="post">
  <div class="modal fade" id="Modaldetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Detail Karyawan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
              <label class="col-form-label">NIP :</label>
              <input type="text" class="form-control" id="nip3" name="nip3">
            </div>
            <div class="form-group">
              <label class="col-form-label">Nama :</label>
              <input type="text" class="form-control" id="nama3" name="nama3">
            </div>
            <div class="form-group">
              <label class="col-form-label">Tempat Lahir :</label>
              <input type="text" class="form-control" id="tempat_lahir3" name="tempat_lahir3">
            </div>
            <div class="form-group">
              <label class="col-form-label">Tanggal Lahir :</label>
              <input type="text" class="form-control datepicker" id="tgl_lahir3" name="tgl_lahir3">
            </div>
            <div class="form-group">
              <label class="col-form-label">Jenis Kelamin :</label>
              <select class="form-control" name="jk3">
                <option value="Laki-laki" selected>Laki-laki</option>
                <option value="Perempuan">Perempuan</option>
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label">Email :</label>
              <input type="email" class="form-control" id="email3" name="email3">
            </div>
            <div class="form-group">
              <label class="col-form-label">No SIM :</label>
              <input type="text" class="form-control" id="no_sim3" name="no_sim3">
            </div>
            <div class="form-group">
              <label class="col-form-label">Alamat :</label>
              <textarea name="alamat3" class="form-control" rows="8" cols="80"></textarea>
            </div>
            <div class="form-group">
              <label class="col-form-label">Jabatan :</label>
              <select class="form-control" name="jabatan3" id="jabatan3">
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label">Foto :</label>
              <div class="" id="viewfoto">

              </div>
            </div>
            <div class="form-group">
              <label class="col-form-label">Status :</label>
              <select class="form-control" name="status3">
                <option value="1" selected>Aktif</option>
                <option value="0">Non-Aktif</option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script src="<?= base_url(); ?>assets/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
var save_method; //for save method string
$(document).ready(function() {
  select();
  var table = $('#table').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "responsive": true,
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "order": [], //Initial no order.
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": "<?php echo base_url('karyawan/ajax_list')?>",
          "type": "POST",
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      {
          "targets": [ 0 ], //first column / numbering column
          "orderable": false, //set not orderable
      },
      ],
  });
});
$('.datepicker').datepicker({
       autoclose: true,
       format: "yyyy-mm-dd",
       todayHighlight: true,
       orientation: "top auto",
       todayBtn: true,
       todayHighlight: true,
});

function eraseText() {
     document.getElementById("nip").value = "";
     document.getElementById("nama").value = "";
     document.getElementById("tempat_lahir").value = "";
     document.getElementById("tgl_lahir").value = "";
     document.getElementById("jk").value = "";
     document.getElementById("alamat").value = "";
     document.getElementById("foto").value = "";
     document.getElementById("status").value = "";
     document.getElementById("email").value = "";
     document.getElementById("no_sim").value = "";
     document.getElementById("password").value = "";
     document.getElementById("thn_lulus").value = "";
     document.getElementById("jabatan").value = "";
}
function eraseTextEdit() {
      document.getElementById("nip2").value = "";
      document.getElementById("nama2").value = "";
      document.getElementById("tempat_lahir2").value = "";
      document.getElementById("tgl_lahir2").value = "";
      document.getElementById("jk2").value = "";
      document.getElementById("alamat2").value = "";
      document.getElementById("foto2").value = "";
      document.getElementById("status2").value = "";
      document.getElementById("email2").value = "";
      document.getElementById("no_sim2").value = "";
      document.getElementById("password2").value = "";
      document.getElementById("thn_lulus2").value = "";
      document.getElementById("jabatan2").value = "";
}
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}
function save()
{
    $('#btnSave').text('Menyimpan...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable
    var url;
    url = "<?php echo site_url('karyawan/tambah')?>";
    // var file_data = $('#foto').prop('files')[0];
    var form_data = new FormData(document.getElementById("tambah"));
    // form_data.append('file', file_data);

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: form_data,
        dataType: "JSON",
        contentType: false,
        processData:false,
        cache:false,
        success: function(data)
        {
    
            $('#pesan').html(data.pesan);
            if(data.pesan=="") //if success close modal and reload ajax table
            {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil ditambahkan!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
                reload_table();
                $('#btnSave').text('simpan'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable
            }
            $('#btnSave').text('simpan'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
        }
    });
}
function edit(id)
{

    $('#ubah')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
      $('#modalEdit').modal('show'); // show bootstrap modal when complete loaded
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url('karyawan/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nip2"]').val(data.nip);
            $('[name="nama2"]').val(data.nama);
            $('[name="tempat_lahir2"]').val(data.tempat_lahir);
            $('[name="tgl_lahir2"]').val(data.tgl_lahir);
            $('[name="jk2"]').val(data.jk);
            $('[name="alamat2"]').val(data.alamat);
            $('[name="email2"]').val(data.email);
            $('[name="no_sim2"]').val(data.no_sim);
            $('[name="jabatan2"]').val(data.jabatan);
            $('[name="foto_lama"]').val(data.foto);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
function detail(id)
{
  $('#Modaldetail').modal('show'); // show bootstrap modal when complete loaded
  img = "<?php echo base_url()?>assets/img/karyawan";
    $.ajax({
        url : "<?php echo site_url('karyawan/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="nip3"]').val(data.nip);
            $('[name="nama3"]').val(data.nama);
            $('[name="tempat_lahir3"]').val(data.tempat_lahir);
            $('[name="tgl_lahir3"]').val(data.tgl_lahir);
            $('[name="jk3"]').val(data.jk);
            $('[name="alamat3"]').val(data.alamat);
            $('[name="email3"]').val(data.email);
            $('[name="no_sim3"]').val(data.no_sim);
            $('[name="jabatan3"]').val(data.jabatan);
            $('[name="foto_lama"]').val(data.foto);
            $('#viewfoto').html('<img src="'+img+'/'+data.foto+'" width="125" height="150" />');
            $('[name="foto3"]').val(data.foto);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
function hapus(id) {
  Swal.fire({
      title: "Apakah anda yakin?",
      text: "data akan dihapus!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          data:'id='+id,
          url:'<?= base_url().'karyawan/hapus'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil dihapus!',
                type: 'success'
            });
              reload_table();
          }
        });
      }
  });
}
function update() {
  $('#btnUpdate').text('Mengubah...'); //change button text
  $('#btnUpdate').attr('disabled',true); //set button disable
  var url = "<?php echo site_url('karyawan/ubah')?>";
  // var file_data = $('#foto').prop('files')[0];
  var form_data2 = new FormData(document.getElementById("ubah"));
  // form_data.append('file', file_data);

  // ajax adding data to database
  $.ajax({
      url : "<?php echo base_url('karyawan/ubah')?>",
      type: "POST",
      data: form_data2,
      dataType: "JSON",
      contentType: false,
      processData:false,
      cache:false,
      success: function(data)
      {
        console.log(data);
          $('#e_pesan').html(data.pesan);
          if(data.pesan=="") //if success close modal and reload ajax table
          {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil diubah!',
                type: 'success'
            });
            setTimeout(function () {
              $("[data-dismiss=modal]").trigger({
                type: "click"
              });
            },100)
              reload_table();
              $('#btnUpdate').text('ubah'); //change button text
              $('#btnUpdate').attr('disabled',false); //set button enable
          }
          $('#btnUpdate').text('ubah'); //change button text
          $('#btnUpdate').attr('disabled',false); //set button enable
      }
  });
}
function select() {
  $.ajax({
    type:'post',
    dataType:'json',
    url:'<?= base_url().'karyawan/getJabatan'?>',
    success:function (data) {
        var html ='';
        var i;
        for (var i = 0; i < data.length; i++) {
          html += '<option value="'+data[i].id+'">'+data[i].nama_jabatan+'</option>';
        }
        $('#jabatan').html(html);
        $('#jabatan2').html(html);
        $('#jabatan3').html(html);
    }
  });
}
</script>
