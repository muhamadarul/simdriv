<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Karyawan</title>
</head>
<body>
    <h3><center>DAFTAR KARYAWAN</center></h3>
    <table border="1" cellspacing="0" cellpadding="5" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Tempat, Tgl Lahir</th>
                <th>Jenis Kelamin</th>
                <th>Email</th>
                <th>Status</th>
                <th>Jabatan</th>
                <th>No SIM</th>
                <th>Alamat</th>
            </tr>
        </thead>
        <tbody>
            <?php if(is_array($karyawan) || is_object($karyawan) ):?>
            <?php
            $no=0;
            foreach ($karyawan as $data) {
                $no++;
                echo "<tr>";
                    echo "<td><center>".$no."</center></td>";
                    echo "<td>".$data['nip']."</td>";
                    echo "<td>".$data['nama']."</td>";
                    echo "<td>".$data['tempat_lahir'].','.$data['tgl_lahir']."</td>";
                    echo "<td>".$data['jk']."</td>";
                    echo "<td>".$data['email']."</td>";
                    if ($data['status']==1) {
                      echo "<td>Aktif</td>";
                    }else {
                      echo "<td>Tidak Aktif</td>";
                    }
                    echo "<td>".$data['nama_jabatan']."</td>";
                    echo "<td>".$data['no_sim']."</td>";
                    echo "<td>".$data['alamat']."</td>";
                echo "</tr>";
            }
            ?>
          <?php endif; ?>

        </tbody>
    </table>
</body>
</html>
