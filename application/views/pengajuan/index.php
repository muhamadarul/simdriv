<!-- MAIN -->
<div class="main">
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <!-- OVERVIEW -->
      <div class="panel panel-headline">
        <div class="panel-heading">
          <h3 class="panel-title">Data Pengajuan Mobil</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12">
              <br>
              <div class="panel">
                <div class="panel-body">
                  <table id="table" class="table table-striped">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>No Pengajuan</th>
                        <th>Tgl Pengajuan</th>
                        <th>Nama</th>
                        <th>Tgl Berangkat</th>
                        <th>Keperluan</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
              <!-- END RECENT PURCHASES -->
            </div>
          </div>
        </div>
      </div>
      <!-- END OVERVIEW -->
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>

<!-- Modal Ubah-->
<form id="ubah" method="post">
  <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Pengajuan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center><font color="red"><p id="e_pesan"></p></font></center>
            <div class="form-group" hidden>
              <label class="col-form-label">Id :</label>
              <input type="text" class="form-control" id="id" name="id">
            </div>
            <div class="form-group">
              <label class="col-form-label">No Pengajuan :</label>
              <input type="text" class="form-control" id="no_pengajuan" name="no_pengajuan" readonly>
            </div>
            <div class="form-group">
              <label class="col-form-label">Tgl Pengajuan :</label>
              <input type="text" class="form-control" id="tgl_pengajuan" name="tgl_pengajuan" readonly>
            </div>
            <div class="form-group">
              <label class="col-form-label">Nama Karyawan :</label>
              <input type="text" class="form-control" id="nama" name="nama" readonly>
            </div>
            <div class="form-group">
              <label class="col-form-label">Tujuan :</label>
              <textarea name="tujuan" class="form-control" id="tujuan" rows="8" cols="80" readonly></textarea>
            </div>
            <div class="form-group">
              <label class="col-form-label">Tgl Berangkat :</label>
              <input type="text" class="form-control" id="tgl_berangkat" name="tgl_berangkat" readonly>
            </div>
            <div class="form-group">
              <label class="col-form-label">Keperluan :</label>
              <textarea name="keperluan" class="form-control" id="keperluan" rows="8" cols="80" readonly></textarea>
            </div>
            <div class="form-group">
              <label class="col-form-label">Status :</label>
              <select class="form-control" name="status" id="status">
                <option value="1">Approved</option>
                <option value="2">Pending</option>
                <option value="0">Rejected</option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" id="#btnUpdate" onclick="update()"  class="btn btn-success"><i class="fa fa-save"></i>&nbsp; Ubah</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Detail-->
<form id="perjalanan" method="post">
  <div class="modal fade" id="Modalperjalanan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Buat Perjalanan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <center><font color="red"><p id="pesan"></p></font></center>
          <table class="table">
            <tr>
              <td>Nama</td>
              <td id="nama2"></td>
            </tr>
            <tr>
              <td>No Pengajuan</td>
              <td id="no_pengajuan2"></td>
            </tr>
            <tr>
              <td>Tgl Pengajuan</td>
              <td id="tgl_pengajuan2"></td>
            </tr>
            <tr>
              <td>Tgl Berangkat</td>
              <td id="tgl_berangkat2"></td>
            </tr>
            <tr>
              <td>Keperluan</td>
              <td id="keperluan2"></td>
            </tr>
          </table>
            <div class="form-group">
              <label class="col-form-label">Pilih Driver :</label>
              <select class="itemDriver form-control" name="driver">
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label">Pilih Kendaraan :</label>
              <select class="itemKendaraan form-control" name="kendaraan">
              </select>
            </div>
            <input type="text" name="pengajuan" id="pengajuan" value="" hidden>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" id="#btnAdd" onclick="add()"  class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script src="<?= base_url(); ?>assets/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
var save_method; //for save method string
$(document).ready(function() {

  var table = $('#table').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "responsive": true,
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "order": [], //Initial no order.
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": "<?php echo base_url('pengajuan/ajax_list')?>",
          "type": "POST",
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      {
          "targets": [ 0 ], //first column / numbering column
          "orderable": false, //set not orderable
      },
      ],
  });
});
function edit(id)
{

    $('#ubah')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
      $('#modalEdit').modal('show'); // show bootstrap modal when complete loaded
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url('pengajuan/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id_pengajuan);
            $('[name="no_pengajuan"]').val(data.no_pengajuan);
            $('[name="tgl_pengajuan"]').val(data.tgl_pengajuan);
            $('[name="nama"]').val(data.nama);
            $('[name="tujuan"]').val(data.tujuan);
            $('[name="tgl_berangkat"]').val(data.tgl_berangkat);
            $('[name="keperluan"]').val(data.keperluan);
            $('[name="status"]').val(data.status_p);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
function perjalanan(id)
{
  $('#Modalperjalanan').modal('show'); // show bootstrap modal when complete loaded
    $.ajax({
        url : "<?php echo site_url('pengajuan/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          $('#pengajuan').val(data.id_pengajuan);
          $('#no_pengajuan2').html(data.no_pengajuan);
          $('#tgl_pengajuan2').html(data.tgl_pengajuan);
          $('#nama2').html(data.nama);
          $('#tgl_berangkat2').html(data.tgl_berangkat);
          $('#keperluan2').html(data.keperluan);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
function update() {
  $('#btnUpdate').text('Mengubah...'); //change button text
  $('#btnUpdate').attr('disabled',true); //set button disable
  var form_data2 = new FormData(document.getElementById("ubah"));
  // form_data.append('file', file_data);

  // ajax adding data to database
  $.ajax({
      url : "<?php echo base_url('pengajuan/ubah')?>",
      type: "POST",
      data: form_data2,
      dataType: "JSON",
      contentType: false,
      processData:false,
      cache:false,
      success: function(data)
      {

          $('#e_pesan').html(data.pesan);
          if(data.pesan=="") //if success close modal and reload ajax table
          {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil diubah!',
                type: 'success'
            });
            setTimeout(function () {
              $("[data-dismiss=modal]").trigger({
                type: "click"
              });
            },100)

              $('#btnUpdate').text('ubah'); //change button text
              $('#btnUpdate').attr('disabled',false); //set button enable
          }
          $('#btnUpdate').text('ubah'); //change button text
          $('#btnUpdate').attr('disabled',false); //set button enable
      }
  });
}

$('.itemDriver').select2({
  ajax:{
    url : '<?= base_url('pengajuan/getDriver'); ?>',
    dataType:"json",
    delay:250,
    data:function(params){
      return {
        driver : params.term
      };
    },
    processResults:function(data){
      var results=[];
      $.each(data, function(index,item){
        results.push({
          id : item.id,
          text : item.nama
        });
      });
      return {
        results:results
      };
    }
  }
});

$('.itemKendaraan').select2({
  ajax:{
    url : '<?= base_url('pengajuan/getKendaraan'); ?>',
    dataType:"json",
    delay:250,
    data:function(params){
      return {
        kendaraan : params.term
      };
    },
    processResults:function(data){
      var results=[];
      $.each(data, function(index,item){
        results.push({
          id : item.id,
          text : item.merk.concat(" - ", item.no_plat)
        });
      });
      return {
        results:results
      };
    }
  }
});


function add() {
  $('#btnAdd').text('Menambahkan...'); //change button text
  $('#btnAdd').attr('disabled',true); //set button disable

  // var file_data = $('#foto').prop('files')[0];
  var form_data = new FormData(document.getElementById("perjalanan"));
  // form_data.append('file', file_data);
  // ajax adding data to database
  $.ajax({
      url : "<?php echo base_url('perjalanan/tambah')?>",
      type: "POST",
      data: form_data,
      dataType: "JSON",
      contentType: false,
      processData:false,
      cache:false,
      success: function(data)
      {
        $('#pesan').html(data.pesan);
        if(data.pesan=="") //if success close modal and reload ajax table
        {
          Swal.fire({
              title: 'Berhasil ',
              text: 'Data berhasil ditambahkan!',
              type: 'success'
          });
          setTimeout(function () {
            $("[data-dismiss=modal]").trigger({
              type: "click"
            });
          },100)
            reload_table();
            $('#btnAdd').text('simpan'); //change button text
            $('#btnAdd').attr('disabled',false); //set button enable
        }
        $('#btnAdd').text('simpan'); //change button text
        $('#btnAdd').attr('disabled',false); //set button enable
      }
  });
}
</script>
