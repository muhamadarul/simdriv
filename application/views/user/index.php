<!-- MAIN -->
<div class="main">
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <!-- OVERVIEW -->
      <div class="panel panel-headline">
        <div class="panel-heading">
          <h3 class="panel-title">Data User</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12">
              <div class="text-right">
                <a href="#" data-toggle="modal" data-target="#modalTambah" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;   Tambah Data</a>
              </div>
              <br>
              <div class="panel">
                <div class="panel-body">
                  <table id="example1" class="table table-striped">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Status</th>
                        <th>Level</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody id="tampildata">

                    </tbody>
                  </table>
                </div>
              </div>
              <!-- END RECENT PURCHASES -->
            </div>
          </div>
        </div>
      </div>
      <!-- END OVERVIEW -->
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>

<!-- Modal Tambah-->
<form id="tambah" method="post">
  <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Tambah Data User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center><font color="red"><p id="pesan"></p></font></center>
            <div class="form-group">
              <label class="col-form-label">Nama :</label>
              <input type="text" class="form-control" id="nama" name="nama">
            </div>
            <div class="form-group">
              <label class="col-form-label">Username :</label>
              <input type="text" class="form-control" id="username" name="username">
            </div>
            <div class="form-group">
              <label class="col-form-label">Status :</label>
              <select class="form-control" name="status">
                <option value="1" selected>Aktif</option>
                <option value="0">Tidak Aktif</option>
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label">Level :</label>
              <select class="form-control" id="level" name="level">
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label">Password :</label>
              <input type="password" class="form-control" id="password1" name="password1">
            </div><div class="form-group">
              <label class="col-form-label">Masukan Ulang Password :</label>
              <input type="password" class="form-control" id="password2" name="password2">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Ubah-->
<form id="ubah" method="post">
  <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Ubah User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center><font color="red"><p id="e_pesan"></p></font></center>
            <div class="form-group" hidden>
              <label class="col-form-label">Id :</label>
              <input type="text" class="form-control" id="e_user" name="e_user">
            </div>
            <div class="form-group">
              <label class="col-form-label">Nama :</label>
              <input type="text" class="form-control" id="e_nama" name="e_nama">
            </div>
            <div class="form-group">
              <label class="col-form-label">Username :</label>
              <input type="text" class="form-control" id="e_username" name="e_username" disabled>
            </div>
            <div class="form-group">
              <label class="col-form-label">Status :</label>
              <select class="form-control" name="e_status">
                <option value="1">Aktif</option>
                <option value="0">Tidak Aktif</option>
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label">Level :</label>
              <select class="form-control" id="e_level" name="e_level">
              </select>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp; Ubah</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
   view();
   select();
});
$(function () {
  $("#example1").DataTable();
});
$('#tambah').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          type:'POST',
          url:"<?= base_url().'user/tambahuser'?>",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#pesan').html(data.pesan);
            if (data.pesan=="") {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil ditambahkan!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
              view();
              erase();
            }
            }
    })
});
function edit(id) {
  $.ajax({
    type:"POST",
    data:'id='+id,
    url:'<?= base_url().'user/getById'?>',
    dataType:'json',
    success:function(data) {
      document.getElementById('e_user').value=data.id;
      document.getElementById('e_nama').value=data.nama;
      document.getElementById('e_username').value=data.username;
      $('[name="e_status"]').val(data.status).trigger('change');
      $('[name="e_level"]').val(data.level).trigger('change');

    }
  });
}
$('#ubah').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          url:"<?= base_url().'user/ubah'?>",
          type:"POST",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#e_pesan').html(data.pesan);
            if (data.pesan=='') {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil diubah!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
          }
          view();
        }
      })
});
function erase() {
  document.getElementById("nama").value = "";
    document.getElementById("username").value = "";
      document.getElementById("status").value = "";
      document.getElementById("level").value = "";
       document.getElementById("password1").value = "";
       document.getElementById("password2").value = "";

}
function view() {
  $.ajax({
    type:'POST',
    url:'<?= base_url().'user/view'?>',
    dataType:'json',
    async:false,
    success:function(data){
      var baris ='';
      var n='';
      var s=[];
      for(var i=0;i<data.length;i++){
        n=i+1;
        if(data[i].status == 1) {
           s[i] = '<td>Aktif</td>';
        }else{
           s[i] = '<td>Non Aktif</td>';
        }
        baris += '<tr>'+
                      '<td>'+  n +'</td>'+
                      '<td>'+ data[i].nama+'</td>'+
                      '<td>'+ data[i].username+'</td>'+
                         s[i] +
                      '<td>'+ data[i].nama_level+'</td>'+

                      '<td><a onclick="edit('+ data[i].id+')" data-toggle="modal" data-target="#modalEdit" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>&nbsp;<a onclick="hapus('+ data[i].id+')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></td>'+
                 '</tr>';
      }
      $('#tampildata').html(baris);
    }
  });
}
function hapus(id) {
  Swal.fire({
      title: "Apakah anda yakin?",
      text: "data akan dihapus!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          data:'id='+id,
          url:'<?= base_url().'user/hapus'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil dihapus!',
                type: 'success'
            });
              view();
          }
        });
      }
  });
}
function select() {
  $.ajax({
    type:'post',
    dataType:'json',
    url:'<?= base_url().'user/getAkses'?>',
    success:function (data) {
        var html ='';
        var i;
        for (var i = 0; i < data.length; i++) {
          html += '<option value="'+data[i].id_akses+'">'+data[i].level+'</option>';
        }
        $('#level').html(html);
        $('#e_level').html(html);


    }
  });
}
</script>
