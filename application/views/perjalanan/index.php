<!-- MAIN -->
<div class="main">
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <!-- OVERVIEW -->
      <div class="panel panel-headline">
        <div class="panel-heading">
          <h3 class="panel-title">Data Perjalanan Mobil</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12">
              <br>
              <div class="panel">
                <div class="panel-body">
                  <table id="table" class="table table-striped">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>No Plat</th>
                        <th>Merk</th>
                        <th>No Pengajuan</th>
                        <th>Tgl Pengajuan</th>
                        <th>Nama</th>
                        <th>Driver</th>
                        <th>Tgl Berangkat</th>
                        <th>Keperluan</th>
                        <th>Tujuan</th>
                        <th>Status Perjalanan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
              <!-- END RECENT PURCHASES -->
            </div>
          </div>
        </div>
      </div>
      <!-- END OVERVIEW -->
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>


<!-- Modal Detail-->
  <div class="modal fade" id="DetailPengeluaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Detail Pengeluaran</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <table class="table">
            <tr>
              <td>No Pengajuan</td>
              <td>:</td>
              <td id="no_pengajuan"></td>
            </tr>
            <tr>
              <td>Tgl Pengajuan</td>
              <td>:</td>
              <td id="tgl_pengajuan"></td>
            </tr>
            <tr>
              <td>No Plat</td>
              <td>:</td>
              <td id="no_plat"></td>
            </tr>
            <tr>
              <td>Merk</td>
              <td>:</td>
              <td id="merk"></td>
            </tr>
            <tr>
              <td>Nama</td>
              <td>:</td>
              <td id="nama"></td>
            </tr>
            <tr>
              <td>Nama Driver</td>
              <td>:</td>
              <td id="nama_driver"></td>
            </tr>
            <tr>
              <td>Tgl Berangkat</td>
              <td>:</td>
              <td id="tgl_berangkat"></td>
            </tr>
            <tr>
              <td>Keperluan</td>
              <td>:</td>
              <td id="keperluan"></td>
            </tr>
            <tr>
              <td>Tujuan</td>
              <td>:</td>
              <td id="tujuan"></td>
            </tr>
            <tr>
              <td>Status</td>
              <td>:</td>
              <td id="status_perjalanan"></td>
            </tr>
          </table>
          <br>
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <td>No</td>
                <td>Nama Pengeluaran</td>
                <td>Jumlah Pengeluaran</td>
              </tr>
            </thead>
            <tbody id="tampildata">

            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
        </div>
      </div>
    </div>
  </div>
<script src="<?= base_url(); ?>assets/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
var save_method; //for save method string
$(document).ready(function() {

  var table = $('#table').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "responsive": true,
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "order": [], //Initial no order.
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": "<?php echo base_url('perjalanan/ajax_list')?>",
          "type": "POST",
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      {
          "targets": [ 0 ], //first column / numbering column
          "orderable": false, //set not orderable
      },
      ],
  });
});

function detail(id) {
  $('#DetailPengeluaran').modal('show'); // show bootstrap modal when complete loaded
    $.ajax({
        data:'id='+id,
        url:'<?= base_url().'perjalanan/getPengeluaran'?>',
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          var baris ='';
          var n='';
          for(var i=0;i<data.length;i++){
            n=i+1;
            baris += '<tr>'+
                          '<td>'+  n +'</td>'+
                          '<td>'+ data[i].nama_pengeluaran+'</td>'+
                          '<td>'+ data[i].jml_pengeluaran+'</td>'+
                     '</tr>';
          }
          $('#tampildata').html(baris);
          $.ajax({
              data:'id='+id,
              url:'<?= base_url().'perjalanan/getDetailPerjalanan'?>',
              type: "POST",
              dataType: "JSON",
              success: function(data2)
              {
                $('#no_plat').html(data2.no_plat);
                $('#merk').html(data2.merk);
                $('#no_pengajuan').html(data2.no_pengajuan);
                $('#tgl_pengajuan').html(data2.tgl_pengajuan);
                $('#nama').html(data2.nama);
                $('#nama_driver').html(data2.nama_driver);
                $('#tgl_berangkat').html(data2.tgl_berangkat);
                $('#nama_driver').html(data2.nama_driver);
                $('#keperluan').html(data2.keperluan);
                $('#tujuan').html(data2.tujuan);
                if (data2.status_perjalanan==3) {
                  $('#status_perjalanan').html('Pending');
                }else if (data2.status_perjalanan==2) {
                  $('#status_perjalanan').html('Sedang Berlangsung');
                }else if (data2.status_perjalanan==1) {
                  $('#status_perjalanan').html('Selesai');
                }else {
                  $('#status_perjalanan').html('Dibatalkan');
                }
              }
          });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
</script>
