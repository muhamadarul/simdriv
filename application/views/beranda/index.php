<!-- MAIN -->
<div class="main">
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <!-- OVERVIEW -->
      <div class="panel panel-headline">
        <div class="panel-heading">
          <h3 class="panel-title">Dashboard</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-4">
              <div class="metric">
                <span class="icon"><i class="fa fa-car"></i></span>
                <p>
                  <span class="number"><?= $vehicle;?></span>
                  <span class="title">Vehicle</span>
                </p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="metric">
                <span class="icon"><i class="fa fa-user"></i></span>
                <p>
                  <span class="number"><?= $driver;?></span>
                  <span class="title">Driver</span>
                </p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="metric">
                <span class="icon"><i class="fa fa-user"></i></span>
                <p>
                  <span class="number"><?= $karyawan;?></span>
                  <span class="title">Karyawan</span>
                </p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel">
                <div class="panel-heading">
                  <h3 class="panel-title">Jadwal Operasional Hari Ini</h3>
                </div>
                <div class="panel-body no-padding">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>No Pengajuan</th>
                        <th>Nama</th>
                        <th>Nama Driver</th>
                        <th>Tujuan</th>
                        <th>Keperluan</th>
                        <th>No Plat</th>
                        <th>Merk</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($perjalanan as $row) : ?>
                      <tr>
                        <td><?= $row['no_pengajuan'];?></td>
                        <td><?= $row['nama'];?></td>
                        <td><?= $row['nama_driver'];?></td>
                        <td><?= $row['tujuan'];?></td>
                        <td><?= $row['keperluan'];?></td>
                        <td><?= $row['no_plat'];?></td>
                        <td><?= $row['merk'];?></td>
                        <?php if ($row['status_perjalanan']==1): ?>
                          <td><span class="label label-success">Selesai</span></td>
                        <?php elseif($row['status_perjalanan']==2): ?>
                          <td><span class="label label-primary">Sedang Berlangsung</span></td>
                        <?php elseif($row['status_perjalanan']==3): ?>
                          <td><span class="label label-warning">Pending</span></td>
                        <?php else: ?>
                          <td><span class="label label-danger">Dibatalkan</span></td>
                        <?php endif; ?>
                      </tr>
                    <?php endforeach;?>
                    </tbody>
                  </table>
                </div>
                <div class="panel-footer">
                  <div class="row">
                    <div class="col-md-12 text-right"><a href="<?= base_url('perjalanan');?>" class="btn btn-primary">Tampilkan Semua</a></div>
                  </div>
                </div>
              </div>
              <!-- END RECENT PURCHASES -->
            </div>
          </div>
        </div>
      </div>
      <!-- END OVERVIEW -->
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>
