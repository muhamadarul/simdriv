<!-- MAIN -->
<div class="main">
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <!-- OVERVIEW -->
      <div class="panel panel-headline">
        <div class="panel-heading">
          <h3 class="panel-title">Laporan Perjalanan Kendaraan</h3>
        </div>
        <div class="panel-body">
          <form method="post" action="<?= base_url('laporan_perjalanan/cetakByTanggal');?>">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for=""> Pilih Tanggal Awal</label>
                <input type="date" name="tgl_awal" class="form-control" value="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for=""> Pilih Tanggal Akhir</label>
                <input type="date" name="tgl_akhir" class="form-control" value="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <button type="submit" class="btn btn-primary"><i class="fa fa-download"></i>&nbsp; Cetak</button>
              </div>
            </div>
          </div>
        </form>
        </div>
      </div>
      <!-- END OVERVIEW -->
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>
