<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Perjalanan</title>
</head>
<body>
    <h3><center>DAFTAR PERJALANAN</center></h3>
    <table border="1" cellspacing="0" cellpadding="5" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>No Plat</th>
                <th>Merk</th>
                <th>No Pengajuan</th>
                <th>Tgl Pengajuan</th>
                <th>Nama</th>
                <th>Nama Driver</th>
                <th>Tgl Berangkat</th>
                <th>Keperluan</th>
                <th>Tujuan</th>
                <th>Pengeluaran</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php if(is_array($perjalanan) || is_object($perjalanan) ):?>
            <?php
            $no=0;
              foreach ($perjalanan as $key ){
                  // $total += $key['jml_pengeluaran'];
                $no++;
                echo "<tr>";
                    echo "<td><center>".$no."</center></td>";
                    echo "<td>".$key['no_plat']."</td>";
                    echo "<td>".$key['merk']."</td>";
                    echo "<td>".$key['no_pengajuan']."</td>";
                    echo "<td>".$key['tgl_pengajuan']."</td>";
                    echo "<td>".$key['nama']."</td>";
                    echo "<td>".$key['nama_driver']."</td>";
                    echo "<td>".$key['tgl_berangkat']."</td>";
                    echo "<td>".$key['keperluan']."</td>";
                    echo "<td>".$key['tujuan']."</td>";
                    echo "<td> Rp. ".$key['total']."</td>";

                    if ($key['status_perjalanan']==3) {
                      echo "<td>Pending</td>";
                    }elseif ($key['status_perjalanan']==1) {
                      echo "<td>Selesai</td>";
                    }elseif ($key['status_perjalanan']==2) {
                      echo "<td>Sedang Berlangsung</td>";
                    }else {
                      echo "<td>Dibatalkan</td>";
                    }
                echo "</tr>";


            }
            ?>
          <?php endif; ?>

        </tbody>
    </table>
</body>
</html>
