<!doctype html>
<html lang="en">

<head>
	<title><?= $title;?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/chartist/css/chartist-custom.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/select2/css/select2.min.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/demo.css">
	<!-- jQuery -->
	<!-- <script src="<?= base_url(); ?>assets/plugins/jquery/jquery.min.js"></script> -->
	<script src="<?= base_url(); ?>assets/plugins/jquery/jquery.js"></script>
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url(); ?>assets/img/apple-icon.png">
	<!-- <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png"> -->
	<!-- summernote -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/summernote/summernote-bs4.css">

	<!-- SweetAlert2 -->
	<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert2.all.min.js"></script>
	<link href="<?= base_url(); ?>assets/plugins/datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
	<!-- DataTables -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
	<script src="<?= base_url(); ?>assets/plugins/select2/js/select2.full.min.js"></script>


</head>
<body>
	<?php $hal= $this->uri->segment(1);	?>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href=""><img src="assets/img/logo-dark.png" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?= base_url();?>assets/img/default.png" class="img-circle" alt="Avatar"> <span><?= $user['nama']; ?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="<?= base_url('profil');?>"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
								<li><a href="<?= base_url('auth/logout');?>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
						<!-- <li>
							<a class="update-pro" href="https://www.themeineed.com/downloads/klorofil-pro-bootstrap-admin-dashboard-template/?utm_source=klorofil&utm_medium=template&utm_campaign=KlorofilPro" title="Upgrade to Pro" target="_blank"><i class="fa fa-rocket"></i> <span>UPGRADE TO PRO</span></a>
						</li> -->
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="<?= base_url('beranda'); ?>" class="<?= ($hal=="beranda") ? 'active' : '' ;?>"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
            <li>
							<a href="#subPages" data-toggle="collapse" class="collapsed <?= ($hal=="vehicle")||($hal=="karyawan")||($hal=="jabatan")||($hal=="driver") ? 'active' : '' ;?>"><i class="lnr lnr-file-empty"></i> <span>Master Data</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="<?= base_url('jabatan'); ?>" class="<?= ($hal=="karyawan") ? 'active' : '' ;?>">Jabatan</a></li>
									<li><a href="<?= base_url('karyawan'); ?>" class="<?= ($hal=="karyawan") ? 'active' : '' ;?>">Karyawan</a></li>
									<li><a href="<?= base_url('driver'); ?>" class="<?= ($hal=="karyawan") ? 'active' : '' ;?>">Driver</a></li>
									<li><a href="<?= base_url('vehicle'); ?>" class="<?= ($hal=="vehicle") ? 'active' : '' ;?>">Vehicle</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#subPages2" data-toggle="collapse" class="collapsed <?= ($hal=="pengajuan")||($hal=="perjalanan") ? 'active' : '' ;?>"><i class="lnr lnr-car"></i> <span>Operasional</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages2" class="collapse ">
								<ul class="nav">
									<li><a href="<?= base_url('pengajuan'); ?>" class="<?= ($hal=="pengajuan") ? 'active' : '' ;?>">Pengajuan</a></li>
									<li><a href="<?= base_url('perjalanan'); ?>" class="<?= ($hal=="perjalanan") ? 'active' : '' ;?>">Perjalanan</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#subPages3" data-toggle="collapse" class="collapsed <?= ($hal=="laporan_pengajuan")||($hal=="") ? 'active' : '' ;?>"><i class="lnr lnr-chart-bars"></i> <span>Laporan</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages3" class="collapse ">
								<ul class="nav">
									<li><a href="<?= base_url('laporan_pengajuan'); ?>" class="<?= ($hal=="laporan_pengajuan") ? 'active' : '' ;?>">Laporan Pengajuan</a></li>
									<li><a href="<?= base_url('laporan_perjalanan'); ?>" class="<?= ($hal=="laporan_perjalanan") ? 'active' : '' ;?>">Perjalanan</a></li>
								</ul>
							</div>
						</li>
            <li><a href="<?= base_url('user');?>" class="<?= ($hal=="user") ? 'active' : '' ;?>"><i class="lnr lnr-users"></i> <span>User</span></a></li>
					</ul>
				</nav>
			</div>
		</div>	<!-- END LEFT SIDEBAR -->
