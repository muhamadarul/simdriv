
<!--begin::Footer-->
<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
  <!--begin::Container-->
  <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
    <!--begin::Copyright-->
    <div class="text-dark order-2 order-md-1">
      <span class="text-muted fw-bold me-1">2021©</span>
      <a href="" target="_blank" class="text-gray-800 text-hover-primary">Template by Keenthemes</a>
    </div>
    <!--end::Copyright-->
    <!--begin::Menu-->
    <ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
      <li class="menu-item">
        <a href="" class="menu-link px-2">Sistem Informasi Kendaraan Operasional</a>
      </li>
    </ul>
    <!--end::Menu-->
  </div>
  <!--end::Container-->
</div>
<!--end::Footer-->
</div>
<!--end::Wrapper-->
</div>
<!--end::Page-->
</div>
<!--end::Root-->


<!--begin::Exolore drawer-->
<div id="kt_explore" class="bg-white" data-kt-drawer="true" data-kt-drawer-name="explore" data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'300px', 'lg': '375px'}" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_explore_toggle" data-kt-drawer-close="#kt_explore_close">
<!--begin::Card-->
<div class="card shadow-none w-100">
<!--begin::Header-->
<div class="card-header" id="kt_explore_header">
<h3 class="card-title fw-bolder text-gray-700">Detail Account</h3>
<div class="card-toolbar">
  <button type="button" class="btn btn-sm btn-icon btn-active-light-primary me-n5" id="kt_explore_close">
    <!--begin::Svg Icon | path: icons/stockholm/Navigation/Close.svg-->
    <span class="svg-icon svg-icon-2">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
        <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
          <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
          <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
        </g>
      </svg>
    </span>
    <!--end::Svg Icon-->
  </button>
</div>
</div>
<!--end::Header-->
<!--begin::Body-->
<div class="card-body" id="kt_explore_body">
<!--begin::Content-->
<div id="kt_explore_scroll" class="scroll-y me-n5 pe-5" data-kt-scroll="true" data-kt-scroll-height="auto" data-kt-scroll-wrappers="#kt_explore_body" data-kt-scroll-dependencies="#kt_explore_header, #kt_explore_footer" data-kt-scroll-offset="5px">
  <!--begin::Demos-->
  <div class="mb-0">
    <!--begin::Menu-->
    <div class="row">
      <div class="col-md-4">
        <div class="cursor-pointer symbol symbol-100px symbol-md-100px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
          <img src="<?= base_url('');?>assets/img/karyawan/<?= $karyawan['foto'];?>" alt="<?= $karyawan['nama'];?>" />
        </div>
      </div>
      <div class="col-md-8">
        <div class="d-flex flex-column">
          <div class="fw-bolder d-flex align-items-center fs-5"><?= $karyawan['nama'];?></div><br>
          <p><?= $karyawan['nip'];?></p>
          <p><?= $karyawan['tempat_lahir'].','.' '.date('d F Y', strtotime($karyawan['tgl_lahir']));?></p>
          <p><?= $karyawan['jk'];?></p>
          <p><?= $karyawan['alamat'];?></p>
          <p><?= $karyawan['no_sim'];?></p>
          <?php if ($karyawan['jabatan']==2): ?>
            <p>Karyawan</p>
          <?php else: ?>
            <p>Driver</p>
          <?php endif; ?>
          <?php if ($karyawan['status']==1):  ?>
            <p>Active</p>
          <?php else: ?>
            <p>Deactive</p>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <!--end::Demos-->
</div>
<!--end::Content-->
</div>
<!--end::Body-->
<!--begin::Footer-->
<div class="card-footer py-5 text-center" id="kt_explore_footer">
<a href="" class="btn btn-primary">Change Profile</a>
</div>
<!--end::Footer-->
</div>
<!--end::Card-->
</div>
<!--end::Exolore drawer-->

<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
  <!--begin::Svg Icon | path: icons/stockholm/Navigation/Up-2.svg-->
  <span class="svg-icon">
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24" />
        <rect fill="#000000" opacity="0.5" x="11" y="10" width="2" height="10" rx="1" />
        <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero" />
      </g>
    </svg>
  </span>
  <!--end::Svg Icon-->
</div>
<!--end::Scrolltop-->
<!--end::Main-->
<!--begin::Javascript-->
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="<?= base_url('');?>assets/metronic/plugins/global/plugins.bundle.js"></script>
<script src="<?= base_url('');?>assets/metronic/js/scripts.bundle.js"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="<?= base_url('');?>assets/metronic/js/custom/widgets.js"></script>
<script src="<?= base_url('');?>assets/metronic/js/script.js"></script>
<script src="<?= base_url('');?>assets/metronic/js/custom/apps/chat/chat.js"></script>
<script src="<?= base_url('');?>assets/metronic/js/custom/modals/create-app.js"></script>
<script src="<?= base_url('');?>assets/metronic/js/custom/modals/upgrade-plan.js"></script>
<!--end::Page Custom Javascript-->
<!--end::Javascript-->
</body>
<!--end::Body-->
</html>
