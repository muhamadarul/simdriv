<!-- MAIN -->
<div class="main">
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <div class="panel panel-profile">
        <div class="clearfix">
          <!-- LEFT COLUMN -->
          <div class="profile-left">
            <!-- PROFILE HEADER -->
            <div class="profile-header">
              <div class="overlay"></div>
              <div class="profile-main">
                <img src="<?= base_url();?>assets/img/default.png" class="img-circle" alt="Avatar" width="80px">
                <h3 class="name"><?= $user['nama']; ?></h3>
                <span class="online-status status-available">Available</span>
              </div>
            </div>
            <!-- END PROFILE HEADER -->
            <!-- PROFILE DETAIL -->
            <div class="profile-detail">
              <div class="profile-info">
                <h4 class="heading">Basic Info</h4>
                <ul class="list-unstyled list-justify">
                  <li>Username <span><?= $user['username']; ?></span></li>
                  <?php if ($user['status'] == 1): ?>
                  <li>Status <span>Aktif</span></li>
                  <?php endif; ?>
                  <li>Level <span><?= $detail['nama_akses']; ?></span></li>
                </ul>
              </div>
            </div>
            <!-- END PROFILE DETAIL -->
          </div>
          <!-- END LEFT COLUMN -->
          <!-- RIGHT COLUMN -->
          <div class="profile-right">
            <h4 class="heading">Ganti Password</h4>
            <!-- TABBED CONTENT -->
              <div class="row">
                <div class="col-md-12 col-sm-12">
                 <?= $this->session->flashdata('message'); ?>
                  <?php echo form_open_multipart('profil'); ?>

                      <div class="form-group">
                          <label for="exampleInputEmail1">Password Lama</label>
                          <input name="password_lama" type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Password">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Password Baru</label>
                          <input name="password_baru1" type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Password">
                      </div>
                      <?= form_error('password_baru1');?>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Ulang Password</label>
                          <input name="password_baru2" type="password" class="form-control" id="exampleInputEmail1" placeholder="Ulangi Password">
                      </div>
                      <?= form_error('password_baru2');?>
                      <input name="id" type="hidden" value="<?= $user['id']; ?>">
                    <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
              </div>
            <!-- END TABBED CONTENT -->
          </div>
          <!-- END RIGHT COLUMN -->
        </div>
      </div>
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
