<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Vehicle</title>
</head>
<body>
    <h3><center>DAFTAR KENDARAAN</center></h3>
    <table border="1" cellspacing="0" cellpadding="5" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>No Plat</th>
                <th>Merk</th>
                <th>Tahun Perakitan</th>
                <th>Isi Silinder</th>
                <th>No Rangka</th>
                <th>No Mesin</th>
                <th>No BPKB</th>
                <th>Warna</th>
            </tr>
        </thead>
        <tbody>
            <?php if(is_array($vehicle) || is_object($vehicle) ):?>
            <?php
            $no=0;
            foreach ($vehicle as $data) {
                $no++;
                echo "<tr>";
                    echo "<td><center>".$no."</center></td>";
                    echo "<td>".$data['no_plat']."</td>";
                    echo "<td>".$data['merk']."</td>";
                    echo "<td>".$data['tahun_perakitan']."</td>";
                    echo "<td>".$data['isi_silinder']."</td>";
                    echo "<td>".$data['no_rangka']."</td>";
                    echo "<td>".$data['no_mesin']."</td>";
                    echo "<td>".$data['no_bpkb']."</td>";
                    echo "<td>".$data['warna']."</td>";
                echo "</tr>";
            }
            ?>
          <?php endif; ?>

        </tbody>
    </table>
</body>
</html>
