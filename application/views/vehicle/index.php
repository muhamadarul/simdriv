<style>
  .ui-datepicker-calendar{
    display: none;
  }
</style>

<!-- MAIN -->
<div class="main">
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <!-- OVERVIEW -->
      <div class="panel panel-headline">
        <div class="panel-heading">
          <h3 class="panel-title">Data Vehicle</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <div class="text-left">
                <a href="<?= base_url('vehicle/download');?>" target="_blank" class="btn btn-danger"><i class="fa fa-download"></i>&nbsp;   Download Data</a>
              </div>
            </div>
            <div class="col-md-6">
              <div class="text-right">
                <a href="#" data-toggle="modal" data-target="#modalTambah" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;   Tambah Data</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <br>
              <div class="panel">
                <div class="panel-body">
                  <table id="example1" class="table table-striped">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>No Plat</th>
                        <th>Merk</th>
                        <th>Tahun Perakitan</th>
                        <th>Isi Silinder</th>
                        <th>No Rangka</th>
                        <th>No Mesin</th>
                        <th>No BPKB</th>
                        <th>Warna</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody id="tampildata">

                    </tbody>
                  </table>
                </div>
              </div>
              <!-- END RECENT PURCHASES -->
            </div>
          </div>
        </div>
      </div>
      <!-- END OVERVIEW -->
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>

<!-- Modal Tambah-->
<form id="tambah" method="post">
  <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Tambah Data Vehicle</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center><font color="red"><p id="pesan"></p></font></center>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-form-label">No Plat :</label>
                  <input type="text" class="form-control" id="no_plat" name="no_plat" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">Merk :</label>
                  <input type="text" class="form-control" id="merk" name="merk" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">Tahun Perakitan :</label>
                  <input type="text" class="form-control assembly-date" id="tahun_perakitan" name="tahun_perakitan" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">Isi Silinder (CC):</label>
                  <input type="number" class="form-control" id="isi_silinder" min="0" name="isi_silinder" style="text-transform:uppercase" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-form-label">No Rangka :</label>
                  <input type="text" class="form-control" id="no_rangka" name="no_rangka" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">No Mesin :</label>
                  <input type="text" class="form-control" id="no_mesin" name="no_mesin" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">No BPKB :</label>
                  <input type="text" class="form-control" id="no_bpkb" name="no_bpkb" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">Warna :</label>
                  <input type="text" class="form-control" id="warna" name="warna" style="text-transform:uppercase" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Ubah-->
<form id="ubah" method="post">
  <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Ubah User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center><font color="red"><p id="e_pesan"></p></font></center>
            <div class="form-group" hidden>
              <label class="col-form-label">Id :</label>
              <input type="text" class="form-control" id="e_vehicle" name="e_vehicle">
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-form-label">No Plat :</label>
                  <input type="text" class="form-control" id="e_no_plat" name="e_no_plat" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">Merk :</label>
                  <input type="text" class="form-control" id="e_merk" name="e_merk" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">Tahun Perakitan :</label>
                  <input type="text" class="form-control assembly-date" id="e_tahun_perakitan" name="e_tahun_perakitan" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">Isi Silinder (CC):</label>
                  <input type="number" class="form-control" id="e_isi_silinder" min="0" name="e_isi_silinder" style="text-transform:uppercase" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-form-label">No Rangka :</label>
                  <input type="text" class="form-control" id="e_no_rangka" name="e_no_rangka" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">No Mesin :</label>
                  <input type="text" class="form-control" id="e_no_mesin" name="e_no_mesin" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">No BPKB :</label>
                  <input type="text" class="form-control" id="e_no_bpkb" name="e_no_bpkb" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                  <label class="col-form-label">Warna :</label>
                  <input type="text" class="form-control" id="e_warna" name="e_warna" style="text-transform:uppercase" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp; Ubah</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script src="<?= base_url(); ?>assets/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   view();
});
$(function () {
  $("#table").DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "responsive": true,
      "processing": true, //Feature control the processing indicator.
  });
});
$('.assembly-date').datepicker({
  minViewMode:2,
  format:'yyyy'
});

$('#tambah').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          type:'POST',
          url:"<?= base_url().'vehicle/tambah'?>",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#pesan').html(data.pesan);
              if (data.pesan=="") {
                Swal.fire({
                    title: 'Berhasil ',
                    text: 'Data berhasil ditambahkan!',
                    type: 'success'
                });
                setTimeout(function () {
                  $("[data-dismiss=modal]").trigger({
                    type: "click"
                  });
                },100)
                view();
                erase();
              }
            }
    })
});
function edit(id) {
  $.ajax({
    type:"POST",
    data:'id='+id,
    url:'<?= base_url().'vehicle/getById'?>',
    dataType:'json',
    success:function(data) {
      document.getElementById('e_vehicle').value=data.id;
      document.getElementById('e_no_plat').value=data.no_plat;
      document.getElementById('e_merk').value=data.merk;
      document.getElementById('e_tahun_perakitan').value=data.tahun_perakitan;
      document.getElementById('e_isi_silinder').value=data.isi_silinder;
      document.getElementById('e_no_rangka').value=data.no_rangka;
      document.getElementById('e_no_mesin').value=data.no_mesin;
      document.getElementById('e_no_bpkb').value=data.no_bpkb;
      document.getElementById('e_warna').value=data.warna;
    }
  });
}
$('#ubah').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          url:"<?= base_url().'vehicle/ubah'?>",
          type:"POST",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#e_pesan').html(data.pesan);
            if (data.pesan=='') {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil diubah!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
          }
          view();
        }
      })
});
function erase() {
  document.getElementById("no_plat").value = "";
  document.getElementById("merk").value = "";
  document.getElementById("tahun_perakitan").value = "";
  document.getElementById("isi_silinder").value = "";
  document.getElementById("no_rangka").value = "";
  document.getElementById("no_mesin").value = "";
  document.getElementById("no_bpkb").value = "";
  document.getElementById("warna").value = "";
}
function view() {
  $.ajax({
    type:'POST',
    url:'<?= base_url().'vehicle/view'?>',
    dataType:'json',
    async:false,
    success:function(data){
      var baris ='';
      var n='';
      var s=[];
      for(var i=0;i<data.length;i++){
        n=i+1;
        baris += '<tr>'+
                      '<td>'+  n +'</td>'+
                      '<td>'+ data[i].no_plat+'</td>'+
                      '<td>'+ data[i].merk+'</td>'+
                      '<td>'+ data[i].tahun_perakitan+'</td>'+
                      '<td>'+ data[i].isi_silinder+'</td>'+
                      '<td>'+ data[i].no_rangka+'</td>'+
                      '<td>'+ data[i].no_mesin+'</td>'+
                      '<td>'+ data[i].no_bpkb+'</td>'+
                      '<td>'+ data[i].warna+'</td>'+
                      '<td><a onclick="edit('+ data[i].id+')" data-toggle="modal" data-target="#modalEdit" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>&nbsp;<a onclick="hapus('+ data[i].id+')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></td>'+
                 '</tr>';
      }
      $('#tampildata').html(baris);
    }
  });
}
function hapus(id) {
  Swal.fire({
      title: "Apakah anda yakin?",
      text: "data akan dihapus!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          data:'id='+id,
          url:'<?= base_url().'vehicle/hapus'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil dihapus!',
                type: 'success'
            });
              view();
          }
        });
      }
  });
}
</script>
