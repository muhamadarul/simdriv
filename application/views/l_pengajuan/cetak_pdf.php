<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Pengajuan</title>
</head>
<body>
    <h3><center>DAFTAR PENGAJUAN</center></h3>
    <table border="1" cellspacing="0" cellpadding="5" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>No Pengajuan</th>
                <th>Tgl Pengajuan</th>
                <th>Nama</th>
                <th>Tgl Berangkat</th>
                <th>Keperluan</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php if(is_array($pengajuan) || is_object($pengajuan) ):?>
            <?php
            $no=0;
            foreach ($pengajuan as $data) {
                $no++;
                echo "<tr>";
                    echo "<td><center>".$no."</center></td>";
                    echo "<td>".$data['no_pengajuan']."</td>";
                    echo "<td>".$data['tgl_pengajuan']."</td>";
                    echo "<td>".$data['nama']."</td>";
                    echo "<td>".$data['tgl_berangkat']."</td>";
                    echo "<td>".$data['keperluan']."</td>";
                    if ($data['status_p']==2) {
                      echo "<td>Pending</td>";
                    }elseif ($data['status_p']==1) {
                      echo "<td>Approved</td>";
                    }else {
                      echo "<td>Rejected</td>";
                    }
                echo "</tr>";
            }
            ?>
          <?php endif; ?>
        </tbody>
    </table>
</body>
</html>
