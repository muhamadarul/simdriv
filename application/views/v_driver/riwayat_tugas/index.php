<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Riwayat tugas</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
                <table id="example2">
                  <thead>
                  <tr>
                    <th>No Pengajuan</th>
                    <th>Nama Penumpang</th>
                    <th>Tujuan</th>
                    <th>Keperluan</th>
                    <th>No Plat</th>
                    <th>Merk</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody id="tampildata">
                    <?php $no = 0; foreach($travel as $row) : $no++ ?>
                    <tr>
                      <td><?= $row['no_pengajuan'];?></td>
                      <td><?= $row['nama'];?></td>
                      <td><?= $row['tujuan'];?></td>
                      <td><?= $row['keperluan'];?></td>
                      <td><?= $row['no_plat'];?></td>
                      <td><?= $row['merk'];?></td>
                      <?php if ($row['status_perjalanan']==1): ?>
                        <td><span class="label label-success">Finished</span></td>
                      <?php elseif($row['status_perjalanan']==2): ?>
                        <td><span class="label label-primary">On progress</span></td>
                      <?php elseif($row['status_perjalanan']==3): ?>
                        <td><span class="label label-warning">Pending</span></td>
                      <?php else: ?>
                        <td><span class="label label-danger">Canceled</span></td>
                      <?php endif; ?>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<script type="text/javascript">
$(function () {
  $("#example1").DataTable();
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "responsive": true,
  });
});

</script>
