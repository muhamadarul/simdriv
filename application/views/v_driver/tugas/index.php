<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tugas Hari Ini</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-body">
          <div class="row">
            <?php if ($tugas != null): ?>
              <?php foreach ($tugas as $row): ?>
              <div class="col-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">No Pengajuan #<?= $row['no_pengajuan'];?></h6>
                  </div>
                  <div class="card-body">
                    <p>Nama : <?= $row['nama'];?></p>
                    <p>Tujuan : <?= $row['tujuan'];?></p>
                    <p>Keperluan : <?= $row['keperluan'];?></p>
                    <p>Merk : <?= $row['merk'];?></p>
                    <p>No Plat : <?= $row['no_plat'];?></p>
                    <?php if ($row['status_perjalanan']==3): ?>
                    <p>Status Perjalanan : PENDING</p>
                    <?php elseif($row['status_perjalanan']==2): ?>
                    <p>Status Perjalanan : ON PROGRESS</p>
                    <?php elseif($row['status_perjalanan']==1): ?>
                    <p>Status Perjalanan : SELESAI</p>
                    <?php else: ?>
                    <p>Status Perjalanan : DIBATALKAN</p>
                    <?php endif; ?>

                    <!-- Pengaturan Kondisi Saat Tombol Di Tekan -->
                    <?php if ($row['status_perjalanan']==3): ?>
                      <a href="#" onclick="berangkat(<?= $row['id_perjalanan'];?>)" class="btn btn-primary btn-icon-split" id="berangkat">
                        <span class="icon text-white-50">
                          <i class="fas fa-arrow-right"></i>
                        </span>
                        <span class="text">Berangkat</span>
                      </a>
                      <div class="my-2"></div>
                      <a href="#" onclick="batal(<?= $row['id_perjalanan'];?>)"  class="btn btn-danger btn-icon-split" id="batal">
                        <span class="icon text-white-50">
                          <i class="fas fa-times"></i>
                        </span>
                        <span class="text">Batalkan</span>
                      </a>
                    <?php elseif($row['status_perjalanan']==2): ?>
                      <div class="my-2"></div>
                      <a class="btn btn-secondary btn-icon-split" id="pengeluaran" onclick="view(<?= $row['id_perjalanan'];?>)" data-toggle="modal" data-target="#modal">
                        <span class="icon text-white-50">
                          <i class="fas fa-money-bill-alt"></i>
                        </span>
                        <span class="text" style="color:white;">Isi Pengeluaran</span>
                      </a>
                      <div class="my-2"></div>
                      <a href="#" onclick="selesai(<?= $row['id_perjalanan'];?>)" class="btn btn-success btn-icon-split" id="selesai">
                        <span class="icon text-white-50">
                          <i class="fas fa-check"></i>
                        </span>
                        <span class="text">Selesai</span>
                      </a>
                    <?php elseif($row['status_perjalanan']==1): ?>

                    <?php else: ?>
                      <div class="my-2"></div>
                      <a href="#" class="btn btn-danger btn-icon-split">
                        <span class="icon text-white-50">
                          <i class="fas fa-times"></i>
                        </span>
                        <span class="text">Tugas Telah Dibatalkan</span>
                      </a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <?php endforeach; ?>
            <?php else: ?>
              <div class="col-12">
                <h4>Tidak Ada Tugas</h4>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->


<!-- Modal Form Penginputan Pengeluaran Driver -->
  <div class="modal fade" id="modal"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Pengeluaran</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-6">
                <form class="" method="post" id="FormPengeluaran">
                  <input type="text" name="id_perjalanan" value="" id="id_perjalanan" hidden>
                  <div class="form-group">
                    <label>Nama Pengeluaran</label>
                    <input type="text" name="nama_pengeluaran" id="nama_pengeluaran" class="form-control" value="">
                  </div>
                  <div class="form-group">
                    <label>Jumlah Pengeluaran</label>
                    <input type="number" name="jml_pengeluaran"  id="jml_pengeluaran"class="form-control" value="">
                  </div>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
              </div>
              <br>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <td>No</td>
                    <td>Nama Pengeluaran</td>
                    <td>Jumlah Pengeluaran</td>
                    <td>Aksi</td>
                  </tr>
                </thead>
                <tbody id="tampildata">

                </tbody>
              </table>
              <br>
              <table>
                <tr>
                  <td>TOTAL</td>
                  <td>: Rp.</td>
                  <td id="total"></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
$(document).ready(function() {
  var id = document.getElementById('id_perjalanan').value;
  view(id);
});
$(function () {
  $("#example1").DataTable();
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "responsive": true,
  });
});
function view(id) {
  $.ajax({
    type:"POST",
    data:'id='+id,
    url:'<?= base_url().'driver_apps/tugas/viewPengeluaran'?>',
    dataType:'json',
    success:function(data) {
      document.getElementById('id_perjalanan').value=id;
        var baris ='';
        var n='';
        var total=0;
        for(var i=0;i<data.length;i++){
          n=i+1;
          total +=parseFloat(data[i].jml_pengeluaran);
          baris += '<tr>'+
                        '<td>'+  n +'</td>'+
                        '<td>'+ data[i].nama_pengeluaran+'</td>'+
                        '<td>'+ data[i].jml_pengeluaran+'</td>'+
                        '<td></a><a onclick="hapus('+ data[i].id+')" class="btn btn-default btn-outline-danger btn-sm"><i class="fa fa-trash"></i></a></td>'+
                   '</tr>';
        }
      $('#tampildata').html(baris);
      $('#total').html(total);
    }
  });
}
function berangkat(id) {
  Swal.fire({
      title: "Perhatian",
      text: "Terima tugas ini ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Terima !'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          data:'id='+id,
          url:'<?= base_url().'driver_apps/tugas/berangkat'?>',
          async:false,
          success : function() {
            Swal.fire({
                title: 'Berhasil',
                text: 'Tugas Ditambahkan!',
                type: 'success'
            });
             $("#berangkat").hide();
             $("#batal").hide();
             setTimeout(window.location.reload.bind(window.location), 500);
          }
        });
      }
  });
}
function batal(id) {
  Swal.fire({
      title: "Perhatian",
      text: "Batalkan tugas ini ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Batalkan !'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          data:'id='+id,
          type:'POST',
          url:'<?= base_url().'driver_apps/tugas/batal'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil',
                text: 'Tugas Dibatalkan!',
                type: 'success'
            });
             $("#berangkat").hide();
             $("#batal").hide();
             $("#selesai").hide();
             setTimeout(window.location.reload.bind(window.location), 500);

          }
        });
      }
  });
}
function selesai(id) {
  Swal.fire({
      title: "Selesai",
      text: "Perjalanan sudah selesai?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Selesai !'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          data:'id='+id,
          url:'<?= base_url().'driver_apps/tugas/selesai'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil',
                text: 'Tugas Ditambahkan!',
                type: 'success'
            });
             $("#selesai").hide();
             $("#pengeluaran").hide();
             setTimeout(window.location.reload.bind(window.location), 500);
          }
        });
      }
  });
}
$('#FormPengeluaran').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          type:'POST',
          url:"<?= base_url().'driver_apps/tugas/simpanPengeluaran'?>",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#pesan').html(data.pesan);
            if (data.pesan=="") {
              var id = document.getElementById('id_perjalanan').value;
              view(id);
              erase();
            }
          }
    })
});
function erase() {
  document.getElementById('nama_pengeluaran').value="";
  document.getElementById('jml_pengeluaran').value="";
}
function hapus(id) {
  Swal.fire({
      title: "Apakah anda yakin?",
      text: "data akan dihapus!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          data:'id='+id,
          url:'<?= base_url().'driver_apps/tugas/hapusPengeluaran'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil dihapus!',
                type: 'success'
            });
            var id = document.getElementById('id_perjalanan').value;
            view(id);
          }
        });
      }
  });
}
</script>
