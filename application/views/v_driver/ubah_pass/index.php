<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Ubah Password</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-body">
              <div class="alert alert-danger alert-dismissible notification">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                  <p id="message"></p>
              </div>
          <div class="row">
            <div class="col-md-6">
              <div class="text-left">
                <form id="ubah" class="" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Password Lama</label>
                        <input name="password_lama" type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Password Baru</label>
                        <input name="password_baru" type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Ulang Password</label>
                        <input name="password_baru2" type="password" class="form-control" id="exampleInputEmail1" placeholder="Ulangi Password">
                    </div>
                    <input name="id" type="hidden" value="<?= $driver['id']; ?>">
                <button type="submit" class="btn btn-primary" name="simpan"><i class="fa fa-save"></i> SIMPAN</button>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<script type="text/javascript">
$(document).ready(function() {
  $('.notification').hide();
});
$('#ubah').on('submit', function(event){
  event.preventDefault();
    $.ajax({
      type:'POST',
      url:"<?= base_url().'driver_apps/ubah_password/action'?>",
      data:$(this).serialize(),
      dataType:'json',
      success:function(data){
        $('.notification').show();
        $('#message').html(data.message);
        if (data.message=="") {
          Swal.fire({
              text: "Password has been updated",
              icon: "success",
              buttonsStyling: false,
              confirmButtonText: "Ok, got it!",
              customClass: {
                  confirmButton: "btn btn-primary"
              }
          })
          $('.notification').hide();
          document.getElementById('password_lama').value = "";
          document.getElementById('password_baru').value = "";
          document.getElementById('password_baru2').value = "";
        }
      }
    })
  });
</script>
