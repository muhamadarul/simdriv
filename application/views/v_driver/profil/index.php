<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Data Diri Driver</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="text-left">
                <table class="table  table-striped">
                  <thead>
                    <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td><?= $driver['nama']; ?></td>
                    </tr>
                    <tr>
                      <td>Tempat, Tgl Lahir</td>
                      <td>:</td>
                      <td><?= $driver['tempat_lahir']; ?>, <?= date('d F Y', strtotime($driver['tgl_lahir'])) ?></td>
                    </tr>
                    <tr>
                      <td>Jenis Kelamin</td>
                      <td>:</td>
                      <td><?= $driver['jk']; ?></td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>:</td>
                      <td><?= $driver['email']; ?></td>
                    </tr>
                    <tr>
                      <td>No SIM</td>
                      <td>:</td>
                      <td><?= $driver['no_sim']; ?></td>
                    </tr>
                    <tr>
                      <td>Alamat</td>
                      <td>:</td>
                      <td><?= $driver['alamat']; ?></td>
                    </tr>
                    <tr>
                      <td>Foto</td>
                      <td>:</td>
                      <td>
                        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width:15rem;" src="<?= base_url(); ?>assets/img/driver/<?= $driver['foto']; ?>" alt="">
                      </td>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
