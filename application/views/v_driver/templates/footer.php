<a id="back-to-top" href="#" class="back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
<!-- Footer -->
<footer class="sticky-footer bg-white">
  <div class="container my-auto">
    <div class="copyright text-center my-auto">
      <span>Copyright &copy; Mr. A <script>document.write(new Date().getFullYear());</script></span>
    </div>
  </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>
      <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="">Logout</a>
              </div>
            </div>
          </div>
        </div>

        <!-- Custom scripts for all pages-->
        <script src="<?= base_url();?>assets/sbadmin/js/sb-admin-2.min.js"></script>
        <!-- Custom Script -->

        <!-- Page level plugins -->
        <!-- <script src="<?= base_url(); ?>assets/dist2/backend/dist/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= base_url(); ?>assets/dist2/backend/dist/vendor/datatables/dataTables.bootstrap4.min.js"></script> -->
        <script src="<?= base_url(); ?>assets/plugins/sweetalert/script.js"></script>

        <!-- Summernote -->
        <script src="<?= base_url(); ?>assets/plugins/summernote/summernote-bs4.min.js"></script>

</body>

</html>
