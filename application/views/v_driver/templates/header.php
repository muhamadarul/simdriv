<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= $title; ?></title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url(); ?>assets/sbadmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/summernote/summernote-bs4.css">

  <!-- Custom styles for this page -->
  <!-- Custom styles for this template-->
  <link href="<?= base_url(); ?>assets/sbadmin/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- SweetAlert2 -->
  <script src="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert2.all.min.js"></script>


  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url();?>assets/sbadmin/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url();?>assets/sbadmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!--  plugin JavaScript-->
  <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js" type="text/javascript"></script> -->
  <script src="<?= base_url();?>assets/sbadmin/vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- DataTables -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>



</head>
<?php $hal= $this->uri->segment(2);
?>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-car"></i>
        </div>
          <div class="sidebar-brand-text mx-1">Ruang Driver</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item <?= ($hal=="beranda") ? 'active' : '' ;?>">
        <a class="nav-link" href="<?= base_url('driver_apps/beranda'); ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <li class="nav-item <?= ($hal=="profile")||($hal=="riwayat_kerja") ? 'active' : '' ;?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapseThree">
          <i class="fas fa-fw fa-car"></i>
          <span>Tugas</span></a>
          <div id="collapse5" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?= base_url('driver_apps/tugas'); ?>">Tugas Hari Ini</a>
              <a class="collapse-item" href="<?= base_url('driver_apps/riwayat_tugas'); ?>">Riwayat Tugas</a>
            </div>
          </div>
      </li>
      <li class="nav-item <?= ($hal=="profil") ? 'active' : '' ;?>">
        <a class="nav-link" href="<?= base_url('driver_apps/profil'); ?>">
          <i class="fas fa-fw fa-user"></i>
          <span>Profil</span></a>
      </li>
      <li class="nav-item <?= ($hal=="ubah_password") ? 'active' : '' ;?>">
        <a class="nav-link" href="<?= base_url('driver_apps/ubah_password'); ?>">
          <i class="fas fa-fw fa-key"></i>
          <span>Ubah Password</span></a>
      </li>



      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <!-- Nav Item - User Information -->

              <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle">
                  <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $driver['nama']; ?></span>
                  <img class="img-profile rounded-circle" src="<?= base_url(); ?>assets/img/driver/<?= $driver['foto']; ?>">
                </a>
              </li>


            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link tombol-logout" href="<?= base_url('driver_apps/login/logout'); ?>">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Logout</span>
                <i class="fa fa-power-off"></i>
              </a>
            </li>
          </ul>

        </nav>
        <!-- End of Topbar -->
