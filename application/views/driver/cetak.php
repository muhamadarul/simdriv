<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Driver</title>
</head>
<body>
    <h3><center>DAFTAR DRIVER</center></h3>
    <table border="1" cellspacing="0" cellpadding="5" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Tempat, Tgl Lahir</th>
                <th>Jenis Kelamin</th>
                <th>Email</th>
                <th>Status</th>
                <th>No SIM</th>
                <th>Alamat</th>
            </tr>
        </thead>
        <tbody>
            <?php if(is_array($driver) || is_object($driver) ):?>
            <?php
            $no=0;
            foreach ($driver as $data) {
                $no++;
                echo "<tr>";
                    echo "<td><center>".$no."</center></td>";
                    echo "<td>".$data['nama']."</td>";
                    echo "<td>".$data['tempat_lahir'].','.$data['tgl_lahir']."</td>";
                    echo "<td>".$data['jk']."</td>";
                    echo "<td>".$data['email']."</td>";
                    if ($data['status']==1) {
                      echo "<td>Aktif</td>";
                    }else {
                      echo "<td>Tidak Aktif</td>";
                    }
                    echo "<td>".$data['no_sim']."</td>";
                    echo "<td>".$data['alamat']."</td>";
                echo "</tr>";
            }
            ?>
          <?php endif; ?>

        </tbody>
    </table>
</body>
</html>
