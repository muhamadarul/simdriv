"use strict";
// DOCS: https://javascript.info/cookie

const successData = $('.success-data').data('success');
const warningData = $('.warning-data').data('warning');
const errorData = $('.error-data').data('error');

if (successData) {
  Swal.fire({
      text: successData,
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "Ok, got it!",
      customClass: {
          confirmButton: "btn btn-primary"
      }
  })
}

if (warningData) {
  Swal.fire({
      text: warningData,
      icon: "warning",
      buttonsStyling: false,
      confirmButtonText: "Ok, got it!",
      customClass: {
          confirmButton: "btn btn-primary"
      }
  })
}

if (errorData) {
  Swal.fire({
      text: errorData,
      icon: "error",
      buttonsStyling: false,
      confirmButtonText: "Ok, got it!",
      customClass: {
          confirmButton: "btn btn-primary"
      }
  })
}


// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTLayoutToolbar.init();
});
